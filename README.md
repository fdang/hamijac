Hamijac
=======
Multi-level parallel Hamilton-Jacobi-Bellman library

## Requirements

+ C++11 compliant compiler


## Optional

+ OpenMP (>= 3.0)
+ MPI
+ CUDA
+ GDAL 1.1

## Usage

./hamijac

see main.cpp for example tests

## Development

Development is done on branch dev.
Master branch should always be stable.


## Unix/Linux

`mkdir build && cd build && cmake ..`  
`make`  
`./hamijac`  

### Options

`make clean` to clean binary  
`make runclean` to clean vtkfiles  
`make distclean` to clean generated CMake files  

### Debug mode

`cmake .. -DCMAKE_BUILD_TYPE=Debug`  

### Release mode

`cmake .. -DCMAKE_BUILD_TYPE=Release`  

### Use different compiler

`CXX=clang++ cmake ..` or
`cmake .. -DCMAKE_CXX_COMPILER=clang++`


### Enable/disable options

`cmake .. -DUSE_OPTIMIZE=OFF` (default:ON)  
`cmake .. -DUSE_OPENMP=OFF`   (default:ON)  
`cmake .. -DUSE_MPI=ON`       (default:OFF)  
`cmake .. -DUSE_SSE3=ON`      (default:OFF)  


### CMake examples

`cmake .. -DCMAKE_BUILD_TYPE=Debug -DUSE_OPTIMIZE=OFF -DUSE_OPENMP=OFF`  
`CXX=clang++ cmake .. -DCMAKE_BUILD_TYPE=Debug -DUSE_OPTIMIZE=OFF -DUSE_OPENMP=OFF`  
`CXX=icpc cmake .. -DCMAKE_BUILD_TYPE=Release -DUSE_SSE3=ON`  


## TODO

- Use std::valarray ?
- Faster map euclidian
- Faster FMM (better heap structure)
- Backtrack gradient descent for path finding

## License

The MIT License (MIT)
see LICENSE file for copying permissions


