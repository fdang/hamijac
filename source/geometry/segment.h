/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef SEGMENT_H_
#define SEGMENT_H_

#include "grid/numerical.h"

//! Functor class : return points around a 2D segments (depending on thickness)
/*!
	This is faster than in Par4HJB since this is not a brute force in O(n^DIM) where
	n is the number of vertices in one dimension.
	We use a bounding box in order to determinate which vertex is inside the ball.
	Example : auto polygon = Segment({1.1,2.0},{2.1,3.0},{1.1,2.0});
	Careful ! up_bound_box coordinates should be higher than corresponding low_bound_box coordinates
*/
template<t_uint DIM, typename RealT, t_uint... v>
class Segment
{
public:
	explicit Segment(const RegGrid<DIM,RealT,v...>& reg_grid) : grid_(reg_grid)
	{
		if (DIM != 2)
			std::cout << "Warning ! Segment will only work in 2D for the moment !" << std::endl;
	}

	bool isAroundSegment(const std::vector<RealT>& real,
		const std::pair<std::vector<RealT>,std::vector<RealT>>& points, RealT thickness)
	{
		// dotprod AB.AC
		RealT dotprod = (real[0] - points.first[0]) * (points.second[0] - points.first[0]) +
			(real[1] - points.first[1]) * (points.second[1] - points.first[1]);
		RealT AB_squared = (points.first[0] - points.second[0]) * (points.first[0] - points.second[0]) +
			(points.first[1] - points.second[1]) * (points.first[1] - points.second[1]);
		RealT AC_squared = (points.first[0] - real[0]) * (points.first[0] - real[0]) +
			(points.first[1] - real[1]) * (points.first[1] - real[1]);

		if (dotprod < 0 || dotprod > AB_squared)
			return false;
		if (AB_squared * AC_squared > thickness * thickness * AB_squared + dotprod * dotprod)
			return false;

		return true;
	}

	std::pair<std::vector<t_uint>,std::vector<t_uint>> findBoundingBoxes(const std::pair<std::vector<RealT>,std::vector<RealT>>& points)
	{
		// DIM should be 2
		std::vector<RealT> min_value(DIM);
		std::vector<RealT> max_value(DIM);
		for (size_t i = 0; i < DIM; i ++)
		{
			min_value[i] = std::min(points.first[i],points.second[i]);
			max_value[i] = std::max(points.first[i],points.second[i]);
		}
		auto coord_low_box = closestVertex(min_value, grid_);
		auto coord_up_box  = closestVertex(max_value, grid_);
		return std::pair<std::vector<t_uint>,std::vector<t_uint>>(coord_low_box,coord_up_box);
	}

	//! Give vertices which are inside a segment with specific thickness
	/*!
		\param points     pair of two vector points which are the extremum of the segment.
		\param thickness  thickness size depends on the problem grid. Should be > step/stride.
		\return return a vector of vertices indexes
	*/
	std::vector<t_uint> operator()(const std::pair<std::vector<RealT>,std::vector<RealT>>& points, RealT thickness)
	{
		std::pair<std::vector<t_uint>,std::vector<t_uint>> bound_box = findBoundingBoxes(points);
		std::vector<t_uint> inside_points;
		auto current = bound_box.first;
		do
		{
			if (isAroundSegment(grid_.coordToReal(current), points, thickness))
			{
				inside_points.push_back(grid_.coordToIndex(current));
			}
		} while (incrementCoords(current, bound_box.first, bound_box.second));

		return inside_points;
	}

private:
	const RegGrid<DIM,RealT,v...>& grid_;
};

template<t_uint DIM, typename RealT, t_uint... v>
Segment<DIM,RealT,v...> createSegment(const RegGrid<DIM, RealT, v...>& grid)
{
	return Segment<DIM,RealT,v...>(grid);
}



#endif // SEGMENT_H_

