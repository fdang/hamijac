/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef POINT_H_
#define POINT_H_

#include "grid/numerical.h"


//! Functor class : inside a sphere in 3D or circle in 2D
template<t_uint DIM, typename RealT, t_uint... v>
class Point
{
public:
	explicit Point(const RegGrid<DIM,RealT,v...>& reg_grid) : grid_(reg_grid) {}

	//! Give index of the center point in the grid
	std::vector<t_uint> operator()()
	{
		std::vector<RealT> real_point(DIM);
		for (size_t i = 0; i < DIM; i ++)
		{
			real_point[i] = grid_.lower()[i] + fabs(grid_.upper()[i] - grid_.lower()[i]) / 2;
		}
		auto closest_vertex = closestVertex(real_point, grid_);
		if (closest_vertex.size() == 0)
			return std::vector<t_uint>();
		return std::vector<t_uint>(1,grid_.coordToIndex(closest_vertex));
	}

	//! Give index of the closest given real point
	std::vector<t_uint> operator()(const std::vector<RealT>& real_point)
	{
		if (real_point.size() != DIM)
				throw std::invalid_argument("Point has not good dimensions !");
		auto closest_vertex = closestVertex(real_point, grid_);
		if (closest_vertex.size() == 0)
			return std::vector<t_uint>();
		return std::vector<t_uint>(1,grid_.coordToIndex(closest_vertex));
	}

	//! Give index of closest several given real points
	std::vector<t_uint> operator()(const std::vector<std::vector<RealT>>& vec_real_point)
	{
		std::vector<t_uint> vec_idx;
		for(std::size_t i = 0; i < vec_real_point.size(); i ++)
		{
			// assert(vec_real_point[i].size() == DIM && "Point definition has not good dimensions !");
			if (vec_real_point[i].size() != DIM)
				throw std::invalid_argument("Point has not good dimensions !");
			auto closest_vertex = closestVertex(vec_real_point[i], grid_);
			if (closest_vertex.size() != 0)
				vec_idx.push_back(grid_.coordToIndex(closest_vertex));
		}
		return vec_idx;
	}

/*
	//! Give index of closest center grid point
	std::vector<t_uint> operator()()
	{

	}
*/

private:
	const RegGrid<DIM,RealT,v...>& grid_;
};


template<t_uint DIM, typename RealT, t_uint... v>
Point<DIM,RealT,v...> createPoint(const RegGrid<DIM, RealT, v...>& grid)
{
	return Point<DIM,RealT,v...>(grid);
}




#endif // POINT_H_

