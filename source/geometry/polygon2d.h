/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef POLYGON_2D_H_
#define POLYGON_2D_H_

#include "grid/numerical.h"


//! Functor class : return points inside a 2D polygon
/*!
	This is faster than in Par4HJB since this is not a brute force in O(n^DIM) where
	n is the number of vertices in one dimension.
	We use a bounding box in order to determinate which vertex is inside the ball.
	Example : auto polygon = Polygon2D({1.1,2.0},{2.1,3.0},{1.1,2.0});
	Careful ! up_bound_box coordinates should be higher than corresponding low_bound_box coordinates
*/
template<t_uint DIM, typename RealT, t_uint... v>
class Polygon2D
{
public:
	explicit Polygon2D(const RegGrid<DIM,RealT,v...>& reg_grid) : grid_(reg_grid)
	{
		if (DIM != 2)
			std::cout << "Warning ! Polygon construction will only work in 2D !" << std::endl;
	}

	bool isInPolygon(const std::vector<RealT>& real_coord, const std::vector<std::vector<RealT>>& poly_vertices)
	{
		bool is_inside = false;
		for(t_uint i = 0, j = poly_vertices.size() - 1; i < poly_vertices.size(); j = i ++)
		{
			if( ((poly_vertices[i][1] >= real_coord[1]) != (poly_vertices[j][1] >= real_coord[1])) &&
				(real_coord[0] <= (poly_vertices[j][0] - poly_vertices[i][0]) * (real_coord[1] - poly_vertices[i][1]) /
				(poly_vertices[j][1] - poly_vertices[i][1]) + poly_vertices[i][0] )
				)
				is_inside = !is_inside;
		}
		return is_inside;
	}

	std::pair<std::vector<t_uint>,std::vector<t_uint>> findBoundingBoxes(const std::vector<std::vector<RealT>>& poly_vertices)
	{
		std::vector<RealT> min_value({poly_vertices[0][0], poly_vertices[0][1]});
		std::vector<RealT> max_value(min_value);
		for (size_t i = 1; i < poly_vertices.size(); i ++)
		{
			if (poly_vertices[i][0] < min_value[0])
				min_value[0] = poly_vertices[i][0];
			else
				max_value[0] = poly_vertices[i][0];

			if (poly_vertices[i][1] < min_value[1])
				min_value[1] = poly_vertices[i][1];
			else
				max_value[1] = poly_vertices[i][1];
		}
		// std::cout << "min max values : " << printContainer(min_value) << printContainer(max_value) << std::endl;
		auto coord_low_box = closestVertex(min_value, grid_);
		auto coord_up_box  = closestVertex(max_value, grid_);
		// std::cout << "coord low up : " << printContainer(coord_low_box) << printContainer(coord_up_box) << std::endl;
		return std::pair<std::vector<t_uint>,std::vector<t_uint>>(coord_low_box,coord_up_box);
	}

	std::vector<t_uint> operator()(const std::vector<std::vector<RealT>>& poly_vertices)
	{
		std::pair<std::vector<t_uint>,std::vector<t_uint>> bound_box = findBoundingBoxes(poly_vertices);
		// std::cout << "bounding " << printContainer(bound_box.first) << ";" << printContainer(bound_box.second) << std::endl;

		std::vector<t_uint> inside_points;
		auto current = bound_box.first;
		do
		{
			if (isInPolygon(grid_.coordToReal(current), poly_vertices))
			{
				inside_points.push_back(grid_.coordToIndex(current));
			}
		} while (incrementCoords(current, bound_box.first, bound_box.second));

		return inside_points;
	}

private:
	const RegGrid<DIM,RealT,v...>& grid_;
};

template<t_uint DIM, typename RealT, t_uint... v>
Polygon2D<DIM,RealT,v...> createPolygon(const RegGrid<DIM, RealT, v...>& grid)
{
	return Polygon2D<DIM,RealT,v...>(grid);
}







#endif // POLYGON_2D_H_

