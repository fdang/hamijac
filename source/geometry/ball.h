/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef BALL_H_
#define BALL_H_

#include "grid/numerical.h"


//! Functor class : return points inside a ball (sphere in 3D or circle in 2D)
/*!
	This is faster than in Par4HJB since this is not a brute force in O(n^DIM) where
	n is the number of vertices in one dimension.
	We use a bounding box in order to determinate which vertex is inside the ball.
	Example : auto circle = Ball({0,1,0},3);
*/
template<t_uint DIM, typename RealT, t_uint... v>
class Ball
{
public:
	explicit Ball(const RegGrid<DIM,RealT,v...>& reg_grid) : grid_(reg_grid) {}

	std::vector<t_uint> operator()(const std::vector<RealT>& center, const RealT& radius)
	{
		if (center.size() != DIM)
			throw std::invalid_argument("Ball definition has not good dimensions !");
		std::vector<RealT> low_bound_box(DIM);
		std::vector<RealT> up_bound_box (DIM);
		// max/min for out of bounds
		// using epsilon to make sure to include border vertices
		for (size_t i = 0; i < DIM; i ++)
			low_bound_box[i] = std::max(grid_.lower()[i] + Constant<RealT>::k_epsilon, center[i] - radius);
		for (size_t i = 0; i < DIM; i ++)
			up_bound_box[i] = std::min(grid_.upper()[i] - Constant<RealT>::k_epsilon, center[i] + radius);

		auto coord_low_box = closestVertex(low_bound_box, grid_);
		auto coord_up_box  = closestVertex(up_bound_box, grid_);

		std::vector<t_uint> inside_points;
		auto current = coord_low_box;

		do
		{
			if (distanceEuclidian(grid_.coordToReal(current), center) <= radius)
			{
				inside_points.push_back(grid_.coordToIndex(current));
			}
		} while (incrementCoords(current, coord_low_box, coord_up_box));

		return inside_points;
	}

private:
	const RegGrid<DIM,RealT,v...>& grid_;
};

template<t_uint DIM, typename RealT, t_uint... v>
Ball<DIM,RealT,v...> createBall(const RegGrid<DIM, RealT, v...>& grid)
{
	return Ball<DIM,RealT,v...>(grid);
}


#endif // BALL_H_

