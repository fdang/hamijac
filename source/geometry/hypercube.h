/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef HYPERCUBE_H_
#define HYPERCUBE_H_

#include "grid/numerical.h"


//! Functor class : return points inside a hypercube (cube in 3D or square in 2D)
/*!
	This is faster than in Par4HJB since this is not a brute force in O(n^DIM) where
	n is the number of vertices in one dimension.
	We use a bounding box in order to determinate which vertex is inside the ball.
	Example : auto cube = Hypercube({0,-2,0},{2,-1,3});
	Careful ! up_bound_box coordinates should be higher than corresponding low_bound_box coordinates
*/
template<t_uint DIM, typename RealT, t_uint... v>
class Hypercube
{
public:
	explicit Hypercube(const RegGrid<DIM,RealT,v...>& reg_grid) : grid_(reg_grid) {}

	std::vector<t_uint> operator()(const std::vector<RealT>& low_bound_box, const std::vector<RealT>& up_bound_box)
	{
		// assert(low_bound_box.size() == DIM && up_bound_box.size() == DIM && "Hypercube definition has not good dimensions !");
		if (low_bound_box.size() != DIM || up_bound_box.size() != DIM)
			throw std::invalid_argument("Hypercube bounds must have good dimensions !");
		for (size_t i = 0; i < DIM; i ++)
		{
			// assert(low_bound_box[i] < up_bound_box[i] && "Upper bound must be higher than lower bound !");
			if (low_bound_box[i] >= up_bound_box[i])
				throw std::domain_error("Upper bound must be strictly higher than lower bound !");
		}
		// max/min for out of bounds
		for (size_t i = 0; i < DIM; i ++)
			low_bound_box[i] = std::max(grid_.lower()[i] + Constant<RealT>::k_epsilon, low_bound_box[i]);
		for (size_t i = 0; i < DIM; i ++)
			up_bound_box[i] = std::min(grid_.upper()[i] - Constant<RealT>::k_epsilon, up_bound_box[i]);

		auto coord_low_box = closestVertex(low_bound_box, grid_);
		auto coord_up_box  = closestVertex(up_bound_box, grid_);

		std::vector<t_uint> inside_points;
		auto current = coord_low_box;

		do
		{
			inside_points.push_back(grid_.coordToIndex(current));
		} while (incrementCoords(current, coord_low_box, coord_up_box));

		return inside_points;
	}


private:
	const RegGrid<DIM,RealT,v...>& grid_;
};


template<t_uint DIM, typename RealT, t_uint... v>
Hypercube<DIM,RealT,v...> createHypercube(const RegGrid<DIM, RealT, v...>& grid)
{
	return Hypercube<DIM,RealT,v...>(grid);
}




#endif // HYPERCUBE_H_

