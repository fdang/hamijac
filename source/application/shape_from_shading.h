/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef SHAPE_FROM_SHADING_H_
#define SHAPE_FROM_SHADING_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdexcept>

#include "solver/problem.h"

namespace application {

template <typename T> 
struct Image
{
	Image(int h, int w) : width(w),height(h),intensity(height * width) {}
	size_t width;
	size_t height;
	std::vector<T> intensity;

	const T& operator[] (const size_t idx) const { return intensity[idx]; };
	T&       operator[] (const size_t idx)       { return intensity[idx]; };

	void print() const { std::cout << printContainer(intensity); }
	std::vector<T> giveVelocity() const
	{
		std::vector<T> vel(height * width);
		for (size_t i = 0; i < vel.size(); i ++)
		{
			vel[i] = std::sqrt(1.0 / (intensity[i] * intensity[i] + std::numeric_limits<T>::epsilon()) - 1);
		}
		return vel;
	}

	std::vector<t_uint> init() const
	{
		T max = 0.0;
		T x;
		std::vector<t_uint> init_vertices;
		// getting the "minimum" local value which should be the max intensity
		for (size_t i = 0; i < intensity.size(); i ++)
		{
			x = intensity[i];
			// std::cout << x << "," << max << "," << (x >= max) << ";";
			if (x >= max)
			{
				if (x == max)
				{
					init_vertices.push_back(i);
				}
				else
				{
					max = x;
					// std::cout << i << ":" << max << ":" << intensity[0] << std::endl;
					init_vertices.clear(); // this is not pretty !
					init_vertices.push_back(i);
				}
			}
		} // end for
		return init_vertices;
	}
};

// let's just read some PGM files http://netpbm.sourceforge.net/doc/pgm.html
template <typename T> 
Image<T> readPGM(const std::string& file)
{
	using std::cout;
	using std::endl;
	int row = 0, col = 0, numrows = 0, numcols = 0, maxval;
	std::ifstream infile(file);
	std::stringstream ss;
	std::string inputLine = "";

	std::cout << file << std::endl;

	std::getline(infile,inputLine);
	if(inputLine.compare("P2") != 0) 
	{
		throw std::invalid_argument("in readPGM() PGM file is not good");
	}
	// else 
	// 	std::cout << "Version : " << inputLine << std::endl;

	std::getline(infile,inputLine);
	// cout << "Comment : " << inputLine << endl;

	ss << infile.rdbuf();
	ss >> numcols >> numrows;
	// cout << numcols << " columns and " << numrows << " rows" << endl;
	Image<T> img(numcols, numrows);

	// int array[numrows][numcols];
	ss >> maxval; // maximum value
	int val;
	T shift = 0.0; 
	T k = (1 - shift) / maxval;

	// explanation $$0->255 and s->1 so f(x)=s + x * k where k = (1-s)/255$$
	for (row = numrows - 1; row >= 0 ; -- row)
	{
		for (col = 0; col < numcols; ++ col)
		{
			ss >> val;
			img[row * numcols + col] = shift + static_cast<T>(val) * k;
			// img[row * numcols + col] = static_cast<T>(val) / 255.0;
		}
	}
	infile.close();

	return img;
}

}

#endif // SHAPE_FROM_SHADING_H_
