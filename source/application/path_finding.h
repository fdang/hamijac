/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef PATH_FINDING_H_
#define PATH_FINDING_H_

#include "solver/problem.h"
#include "grid/numerical.h"

namespace application
{

// template<typename T>
// T sgn(T d) { 
// 	if (d < -std::numeric_limits<T>::epsilon()) 
// 		return -1;
// 	else 
// 		return d > std::numeric_limits<T>::epsilon(); 
// }

template <typename T> 
T sgn(T val) {
	return (T(0) < val) - (val < T(0));
}


/*!
 This function gives a path from the nearest source point and the destination point (in real coordinates)
 
*/
template<t_uint DIM, typename RealT, t_uint... v>
vector<RealT> gradientDescent(const vector<RealT>& destination, const Problem<DIM, RealT, v...>& problem)
{
	vector<RealT> res(problem.num_vertices(), 0.0); // result map

	// gradient map
	vector<RealT> grads(DIM, 0.0);
	t_uint idx = problem.realToIndex(destination);

	vector<RealT> current_point(destination);
	// vector<t_uint> current_coord(problem.indexToCoord(idx));
	vector<vector<RealT>> path; // the path

	RealT time = 0.0;
	path.push_back(current_point);
	res[idx] = time;

	const double step = problem.stride()[0];

	while (problem.u()[idx] > Constant<RealT>::k_epsilon)
	{
		vector<t_int> neighbors = problem.firstOrderPointStencil(idx);

		// grads[0] = - problem.u()[neighbors[0]] / 2 + problem.u()[neighbors[1]] / 2;
		// if (std::isinf(grads[0]))
		// 	grads[0] = sgn<RealT>(grads[0]);
		// RealT max_grad = std::abs(grads[0]);
		RealT max_grad = 0.0;

		for (t_uint d = 0; d < DIM; d ++)
		{
			grads[d] = 0.0;
			grads[d] -= problem.u()[neighbors[2 * d]] / 2;
			grads[d] += problem.u()[neighbors[2 * d + 1]] / 2;
			if (std::isinf(grads[d]))
				grads[d] = sgn<RealT>(grads[d]);
			if (std::abs(max_grad) < std::abs(grads[d]))
				max_grad = grads[d];
		}

		// Updating points
		for (t_uint d = 0; d < DIM; d ++)
		{
			current_point[d] = current_point[d] - step * grads[d] / std::abs(max_grad);
			// current_coord[i] = current_point[i];
		}

		path.push_back(current_point);



		// path_velocity.push_back(grid[idx].getVelocity());
		// idx = problem.coordToIndex(current_coord);
		// idx = problem.realToIndex(current_point);
		RealT new_idx = problem.coordToIndex(closestVertex(current_point, problem.grid()));
		if (idx == new_idx)
		{
			printf("Warning ! stuck on same point");
			break;
		}
		idx = new_idx;

		// std::cout << idx << " : " << problem.u()[idx] << ";" << printContainer(current_point) << std::endl;
		time += 0.2;
		res[idx] = time;
	}
	// path.push_back(current_point);
	// path_velocity.push_back(grid[idx].getVelocity());

	return res;
}


template<t_uint DIM, typename RealT, t_uint... v>
vector<RealT> findSimplePath(const vector<RealT>& destination, const Problem<DIM, RealT, v...>& problem)
{
	vector<RealT> res(problem.num_vertices(),0);

	t_uint dest = problem.realToIndex(destination);
	RealT min_value = problem[dest];
	t_uint current_idx = dest;
	RealT time = 0.0;

	while (min_value > Constant<RealT>::k_epsilon)
	{
		vector<t_int> neighbors = problem.firstOrderPointStencil(current_idx);
		t_uint min_neighbor = 4 * DIM;
		for (auto neighbor : neighbors)
		{
			if (neighbor >= 0)
			{
				if (problem[neighbor] < min_value)
				{
					min_value = problem[neighbor];
					min_neighbor = neighbor;
				}
			}
		}
		if (current_idx == min_neighbor)
		{
			printf("Warning ! stuck same min value");
			break;
		}
		current_idx = min_neighbor;
		res[current_idx] = time;
		time += 0.2;
	}

	return res;
}

// deep : 2
template<t_uint DIM, typename RealT, t_uint... v>
vector<RealT> findBacktrackPath(const vector<RealT>& destination, const Problem<DIM, RealT, v...>& problem)
{
	vector<RealT> res(problem.num_vertices(),0);

	t_uint dest = problem.realToIndex(destination);
	RealT min_value = problem[dest];
	t_uint current_idx = dest;
	RealT time = 0.0;

	while (min_value > Constant<RealT>::k_epsilon)
	{
		vector<t_int> neighbors = problem.firstOrderPointStencil(current_idx);
		t_uint min_neighbor = 4 * DIM;
		for (auto neighbor : neighbors)
		{
			if (neighbor >= 0)
			{
				if (problem[neighbor] < problem[current_idx])
				{
					vector<t_int> neighbors_deep1 = problem.firstOrderPointStencil(neighbor);
					for (auto neighbor_deep1 : neighbors_deep1)
					{
						if (neighbor_deep1 >= 0)
						{
							if (problem[neighbor_deep1] < problem[neighbor])
							{
								min_value = problem[neighbor_deep1];
								min_neighbor = neighbor;
							}
						}
					}
				}
			}
		}
		if (current_idx == min_neighbor)
		{
			printf("Warning ! stuck same min value");
			break;
		}
		current_idx = min_neighbor;
		res[current_idx] = time;
		time += 0.2;
	}

	return res;
}


} // end namespace applications

#endif // PATH_FINDING_H_

