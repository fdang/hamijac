/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef BINARY_HEAP_H_
#define BINARY_HEAP_H_

#include <algorithm> // make_heap, push_heap, pop_heap

//! A binary min heap structure used for the FMM using STL for convenience
/*!
	So that the smallest value can be easily detected.
    https://en.wikipedia.org/wiki/Heap_(data_structure)
 */
template <class T, class cmp=std::greater<T>>
class BHeap
{
public:
	typedef typename std::vector<T>::value_type      value_type;
	typename std::vector<T>::iterator begin(){ return data_.begin(); } // for printContainer
	typename std::vector<T>::iterator end()  { return data_.end(); }   // for printContainer
	typename std::vector<T>::const_iterator begin() const { return data_.cbegin(); } // for printContainer
	typename std::vector<T>::const_iterator end()   const { return data_.cend();   }   // for printContainer

	BHeap() : data_() {};
	explicit BHeap(std::vector<T> data) : data_(data) { heapify(); }
	template<class Iter>
	BHeap(Iter start, Iter end) : data_(start, end) { std::make_heap(start, end, cmp()); }

	size_t size() const { return data_.size(); }
	bool empty() const { return data_.empty(); }

	//! get min or max. Complexity is O(1).
	T extract()  const { return data_.front(); } // or data_[0]

	//! delete root often call after extract(). Complexity is O(log(n)).
	void pop() { std::pop_heap(data_.begin(), data_.end(), cmp()); data_.pop_back(); }

	//! Insertion. Complexity is O(complexity of push_back + log(n)).
	void insert(T element) { data_.push_back(element); std::push_heap(data_.begin(), data_.end(), cmp()); }

	//! decreaseKey() is O(N.log(N)) for the moment
	/*! Could be O(log(N)) but need an other vector to keep track of index in heap according to given index in grid.
		TODO :  Implement this index tracking is surely the way to go. This will imply to make a binary heap from scratch with stl.
	 */
	bool changeKey(const T& element, const T& change) 
	{
		size_t i = 0;
		while (element != data_[i] && i < size())
			i ++;
		if (i == size())
		{
			std::cerr << "Error ! changeKey element was not found !" << std::endl;
			return false;
		}
		data_[i] = change;
		heapify(); 
		return true;
	}

	//! Heapify vector data_. Should not be called outside class.
	void heapify() { std::make_heap(data_.begin(), data_.end(), cmp()); }	

	// should be O(log n)
	// template <class Iter>
	// void delete_element(Iter element, Iter start, Iter end)
	// {
	// 	// element->key = something that would compare less to everything;
	// 	std::push_heap(start, element + 1, cmp());
	// 	std::pop_heap(start, end, cmp());
	// }

	//! Change a value. Complexity is O(N). 
	// void changeKey(size_t pos, const T& change) { data_[pos] = change; heapify(); }
	// template <class Iter>
	// void changeKey(Iter element, const T& change) { *element = change; heapify(); }

	// void increaseKey(size_t pos, const T& change);
	// void decreaseKey(size_t pos, const T& change);
	// void siftUp(size_t node);
	// void siftDown(size_t node);


private:
	std::vector<T> data_;
};



#endif  // BINARY_HEAP_H_

