/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef REGULAR_GRID_
#define REGULAR_GRID_

#include <climits> // INT_MAX
#include <cassert>
#include <vector>
#include <functional>  // multiplies
#include <numeric>     // accumulate
#include <algorithm>   // generate
#include <stdexcept>   // invalid_argument


#include "tools/functional_style.h"
#include "tools/typedefs.h"


#define ON_STACK_ // using stack memory for vertices per dimension


//! Multi dimensional regular grid
/*!
	This regular grid class intends to be generic and fast.
	RegGrid<DIM,RealT, ...v> e.g. RegGrid<2,float,3,4> grid({-1,-2},{2,3})
	DIM is the grid dimension
	RealT is the floating type
	...v are the number of vertices for each dimension

	Using dim_ as template in RegGrid gives 0.31 s instead of 0.36 s
*/
template<t_uint DIM=2, typename RealT=double, t_uint... v>
class RegGrid
{
public:
	// static constexpr vector<t_uint> k_outofbounds = vector<t_uint>(DIM,-1);

	//! Classic regular grid constructor
	/*!
		{
		  {lower[0], ..., upper[0]];
		  {lower[1], ..., upper[1]];
		  ...;
		  {lower[dims-1], ..., upper[dims-1]];
		}
	*/
	RegGrid(const vector<RealT>& lower, const vector<RealT>& upper) :
#ifdef ON_STACK_
		vertices_{v...},
		num_vertices_(set_num_vertices()),
#else
		vertices_(set_default_vertices()),
		num_vertices_(std::accumulate(vertices_.begin(), vertices_.end(), 1, std::multiplies<t_uint>())),
#endif
		lower_(lower),
		upper_(upper),
		stride_(set_default_strides()) { }

	//! Regular grid constructor : lower bounds are set to low and upper bounds are set to up
	RegGrid(RealT low, RealT up) : RegGrid(vector<RealT>(DIM,low), vector<RealT>(DIM,up)) {}

	//! Default regular grid constructor : creates a [-1;1]^dims grid
	RegGrid() : RegGrid(-1.0,1.0) {}

	// const t_uint& dim() const { return DIM; }
	t_uint num_vertices() const { return num_vertices_; }
	// const vector<t_uint>& vertices() const { return vertices_; }
	t_uint vertices_per_dir(t_uint dim) const { return vertices_[dim]; }
	const vector<RealT>& lower()  const { return lower_; }
	const vector<RealT>& upper()  const { return upper_; }
	const vector<RealT>& stride() const { return stride_; }

	// 1000 for 2D, 100 for 3D, and 10 for above...
	t_uint default_num_vertices() const { return std::pow(10,std::max(static_cast<int>(5-DIM),1)); }

#ifdef ON_STACK_
	t_uint set_num_vertices()
	{
		if (sizeof...(v) == 0 || sizeof...(v) == 1)
		{
			t_uint v_default = default_num_vertices();
			if (sizeof...(v) == 1) v_default = vertices_[0];
			for (t_uint i = 0; i < DIM; i ++)
					vertices_[i] = v_default;
			return std::pow(v_default,DIM);
		}
		else if (sizeof...(v) != DIM)
		{
			std::cerr << Message::m_err_template << std::endl;
			return 0;
		}
		t_uint prod = 1;
		for(t_uint i = 0; i < DIM; i ++)
			prod *= vertices_[i];
		return prod;
	}
#endif

#if !defined(ON_STACK_)
	const vector<t_uint> set_default_vertices() const
	{
		t_uint num_v = sizeof...(v); // or vec.size()
		vector<t_uint> vertices = {v...};
		if (num_v == 0)
			return vector<t_uint>(DIM,default_num_vertices());
		else if (num_v == 1)
			return vector<t_uint>(DIM,vertices[0]);
		else if (num_v == DIM)
			return vertices;
		else
		{
			std::cerr << Message::m_err_template << std::endl;
			return vector<t_uint>();
		}
	}
#endif

	//! Set the default strides based on the lower and upper bounds
	/*!
		Throw an exception if any low bound is higher than high bound.
	*/
	const vector<RealT> set_default_strides() const
	{
		vector<RealT> stride(DIM);
		for (t_uint i = 0; i < DIM; ++ i)
		{
			//assert(upper_[i] > lower_[i] && "Assert ! Upper bound must be higher than lower bound...");
			if(upper_[i] <= lower_[i])
				throw std::domain_error("Upper bound must be strictly higher than lower bound !");
			stride[i] = (upper_[i] - lower_[i]) / (vertices_[i] - 1);
		}
		return stride;
	}

	//! Give real coordinates values from grid coordinates
	/*!
		(i,j,...)->(x,y,...)
	*/
	const vector<RealT> coordToReal(const vector<t_uint>& coord) const
	{
		assert(coord.size() == DIM);
		vector<RealT> real_coord(DIM);
		for (t_uint i = 0; i < DIM; ++ i)
			real_coord[i] = lower_[i] + static_cast<RealT>(coord[i]) * stride_[i];
		return real_coord;
	}

	//! Give grid coordinates from grid index
	/*!
		idx->(i,j,...)
	*/
	const vector<t_uint> indexToCoord(const t_uint& idx) const
	{
		vector<t_uint> coord(DIM);
		t_uint a = idx, b = num_vertices_, r = 0;	//a = b.q + r

		for (t_uint i = 0; i <= DIM - 2; i ++)
		{
			b /= vertices_[i];
			coord[DIM - i - 1] = a / b;
			r = a % b;
			a = r;
		}
		coord[0] = r;
		return coord;
	}

	//! Give index from grid coordinates
	/*!
		(i,j,...)->idx
	*/
	t_uint coordToIndex(const vector<t_uint>& coord) const
	{
		t_uint idx = coord[0];
		t_uint prod = 1;

		for (t_uint dim = 1; dim < DIM; dim ++)
		{
			prod *= vertices_[dim];
			idx += coord[dim] * prod;
		}
		return idx;
	}

	//! Give real coordinates from grid index
	/*!
		idx->(x,y,...)
	*/
	const vector<RealT> indexToReal(const t_uint& idx) const { return coordToReal(indexToCoord(idx)); }


	//! Give index from real coordinates
	/*!
		(x,y,...)->idx
	*/
	t_uint realToIndex(const vector<RealT>& real) const
	{
		t_int idx = 0;

		for (t_uint i = 0; i < DIM - 1; i++)
			idx = (idx + static_cast<t_uint>(ceil((- lower_[i] + real[i]) / stride_[i]))) * vertices_[i + 1];
		idx += static_cast<t_uint>(ceil((- lower_[DIM-1] +
			real[DIM-1]) / stride_[DIM-1]));
		
		// std::cout << printContainer(real) << "->" << idx << std::endl;
		return static_cast<t_uint>(idx);
	}


	//! Give vector of neighbours grid coordinates at a given vertice and dimension
	/*!
		idx ->{() ; (); ...}
		TODO : allocating and freeing memory take time. Must pass vector of neighbors in parameters !
	*/
	const vector<t_int> firstOrderPointStencil(const t_uint& idx) const
	{
		vector<t_int> neighbors(2 * DIM);
		const vector<t_uint> coord(indexToCoord(idx));
		t_uint shift = 1;
		for (t_uint i = 0; i < DIM; i ++)
		{
			if(coord[i] > 0)
				neighbors[2*i] = idx - shift;
			else
				neighbors[2*i] = -1;

			if(coord[i] < (vertices_[i] - 1) && (idx + shift) < num_vertices_)
				neighbors[2*i + 1] = idx + shift;
			else
				neighbors[2*i + 1] = -1;

			shift *= vertices_[i];
		}
		return neighbors;
	}


	//! Give vector of neighbours grid coordinates at a given vertice and dimension 2nd order
	/*!
		idx ->{() ; (); ...}
		TODO : allocating and freeing memory take time. Must pass vector of neighbors in parameters !
	*/
	const vector<t_int> secondOrderPointStencil(const t_uint& idx) const
	{
		vector<t_int> neighbors(4 * DIM);
		const vector<t_uint> coord(indexToCoord(idx));
		// std::cout << printContainer(coord) << " ";
		t_uint shift = 1;
		for (t_uint i = 0; i < DIM; i ++)
		{
			if(coord[i] > 0)
			{
				if (coord[i] > 1)
					neighbors[4*i] = idx - 2 * shift;
				else
					neighbors[4*i] = -1;
				neighbors[4*i+1] = idx - shift;			
			}
			else
			{
				neighbors[4*i] = -1;
				neighbors[4*i+1] = -1;
			}
			if(coord[i] < (vertices_[i] - 1) && (idx + shift) < num_vertices_)
			{
				if (coord[i] < (vertices_[i] - 2) && (idx + 2 * shift) < num_vertices_)
					neighbors[4 * i + 3] = idx + 2 * shift;
				else
					neighbors[4 * i + 3] = -1;
				neighbors[4*i + 2] = idx + shift;
			}
			else
			{
				neighbors[4*i + 2] = -1;
				neighbors[4*i + 3] = -1;
			}

			shift *= vertices_[i];
		}
		return neighbors;
	}


	//! Give real coordinates on one dimension
	/*
	   Called by saveVTK
	*/
	const vector<RealT> giveRealOneDim(const t_uint& dim) const
	{
		vector<RealT> real_coord(vertices_[dim]);
		RealT x(lower_[dim]-stride_[dim]);
		std::generate(real_coord.begin(), real_coord.end(), [&]{ return x += stride_[dim]; } );
		return real_coord;
	}

	//! Give all grid coordinates
	//*! This function is not recommended to be used !
	const std::vector<vector<t_uint>> giveAllCoord() const
	{
		assert(num_vertices_ < INT_MAX / std::pow(DIM, DIM - 1) && "Assert : grid is too big to use giveAllCoord() !");
		std::vector<vector<t_uint>> vec(num_vertices_, vector<t_uint>(DIM));
		t_uint i(0);
		std::generate(vec.begin(), vec.end(), [&]{ return indexToCoord(i ++); });
		return vec;
	}

	//! Give all real grid coordinates
	//*! This function is not recommended to be used !
	const std::vector<vector<RealT>> giveAllReal() const
	{
		assert(num_vertices_ < INT_MAX / std::pow(DIM, DIM - 1) && "Assert : grid is too big to use giveAllReal() !");
		std::vector<vector<RealT>> real_vec(num_vertices_, vector<RealT>(DIM));
		t_uint i(0);
		std::generate(real_vec.begin(), real_vec.end(), [&]{ return indexToReal(i ++); });
		return real_vec;
	}

	//! Pretty info
	void print() const
	{
#ifdef ON_STACK_
		std::cout << "(i) RegGrid " << DIM << "D | ";
		for (size_t i = 0; i < DIM; i ++) std::cout << vertices_[i] << ":";
#else
		std::cout << "(i) RegGrid " << DIM << "D | vertices" << printContainer(vertices_);
#endif
		std::cout << "->" << num_vertices_ << " in total | lower" << printContainer(lower_);
		std::cout << " upper" << printContainer(upper_) << " | stride";
		std::cout << printContainer(stride_) << std::endl;
	}

private:
#ifdef ON_STACK_
	t_uint                        vertices_[DIM]; /*!< Number of vertices per dimension */
#else
	const vector<t_uint>              vertices_;  /*!< Number of vertices per dimension */
#endif
	const t_uint                  num_vertices_;  /*!< Total number of vertices */
	const vector<RealT>              lower_;         /*!< Lower bounds */
	const vector<RealT>              upper_;         /*!< Upper bounds */
	const vector<RealT>              stride_;        /*!< Stride in each dimension */
};

#endif  // MULTI_DIM_GRID_H_

