/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef NUMERICAL_H_
#define NUMERICAL_H_

#include <algorithm>    // std::lower_bound, std::upper_bound
#include <vector>       // std::vector
#include <cmath>        // std::sqrt
#include <stdexcept>

// #include "tools/typedefs.h"
#include "grid/regular_grid.h"


//! This function allows to have variadic nested loops for any dimension levels
/*!
    Used for ball, hypercube, polygons
*/
bool incrementCoords(std::vector<t_uint>& current, const std::vector<t_uint>& lower, const std::vector<t_uint>& upper)
{
    for (auto i = current.size(); i != 0;)
    {
        i --;
        ++ current[i];
        if (current[i] != upper[i] + 1)
        {
            return true;
        }
        current[i] = lower[i];
    }
    return false;
}


template<typename T>
T distanceEuclidian(const std::vector<T>& x, const std::vector<T>& y)
{
	size_t size = x.size();
	assert(size == y.size() && "Vectors should be of same size for computing Euclidian distance !");
	T dist = 0;
	for (t_uint i = 0; i < size; i ++)
	{
		T diff = (x[i] - y[i]);
		dist += diff * diff;
	}
	return std::sqrt(dist);
}


//! Find the index of the nearest element of a given value in a vector.
/*!
  Returns -1 if value is not inside bounds of the grid
  Complexity is O(log(n)). An other way is to use a binary tree search.
*/
template<typename T>
t_int closestValue(T value, const std::vector<T>& vec)
{
	if (value < vec[0])  { std::cout << "< down " << value << ":" << vec[0] << std::endl; return -1; }  // if out of the box (< lower bound)
	auto const it = std::lower_bound(vec.begin(), vec.end(), value);
	if (it == vec.end()) { std::cout << "> up " << value << ":" << *it  << std::endl;  return -1; }  // if out of the box (> upper bound)

	t_int idx = it - vec.begin();
	if (value - vec[idx-1] > vec[idx] - value)  // which is closest low or up ?
		return idx;
	else
		return idx - 1;
}


//! Find the grid coordinates of nearest vertex of a given point (with real coordinates)
/*!
	Returns (-1,-1,...,-1) vector if any closest value is not found
    ex : auto coord_closest = closestVertex(grid, {0.2,1.2,-2.1});
*/
template<t_uint DIM, typename RealT, t_uint... v>
std::vector<t_uint> closestVertex(const std::vector<RealT>& vec_value, const RegGrid<DIM,RealT,v...>& reg_grid)
{
	assert(vec_value.size() == DIM && "vec_values must have DIM size !"); // do not use exception since it should not be a user error.
	std::vector<t_uint> vertex(DIM);
	for(size_t dim = 0; dim < DIM; dim ++)
	{
		auto vec_one_dim = reg_grid.giveRealOneDim(dim);
		int close_val = closestValue(vec_value[dim],vec_one_dim);
		if (close_val < 0)  // out of bonds ?
		{
			std::cout << "Warning ! closestVertex returns out of bounds on " << printContainer(vec_value) << std::endl;
			return std::vector<t_uint>();             //  close_val should be -1
		}
		vertex[dim] = static_cast<t_uint>(close_val); // should be safe
	}
	return vertex;
}



#endif // NUMERICAL_H_

