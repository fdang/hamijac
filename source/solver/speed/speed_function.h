/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef SPEED_H_
#define SPEED_H_

#include "grid/regular_grid.h"

namespace velocity
{
template<t_uint DIM, typename RealT, t_uint... v>
RealT speedChacon(t_uint idx, const RegGrid<DIM, RealT, v...>& reg_grid)
{
	/*
	// [JW] for 2D
	// UintT x = idx / n_;
	// UintT y = idx % n_;

	UintT x = idx % x_size_;
	UintT y = (idx / x_size_) % y_size_;
	UintT z = idx / (x_size_ * y_size_);
	//    std::cout << x << " " << y << " " << z << std::endl;

	// Chacon et al. "A parallel Heap-Cell Method for Eikonal equations", 2014.
	return pow(1.0 + 0.5  * sin (20.0 * PI_ * x * step_) * sin (20.0 * PI_ * y * step_) * sin (20.0 * PI_ * z * step_), 2.0);
	*/
	// This should be generic for any dimensions
	auto coord = reg_grid.indexToCoord(idx);
	RealT prod = 0.5 * std::sin(20.0 * Constant<RealT>::k_pi * coord[DIM - 1] * reg_grid.stride()[DIM - 1]);
	// std::cout << "idx = " << idx << " coord=" << printContainer(coord) << " prod=" << prod << std::endl;
	for (size_t dim = 1; dim < DIM; dim ++)
		prod *= std::sin(20.0 * Constant<RealT>::k_pi * coord[DIM - 1 - dim] * reg_grid.stride()[DIM - 1 - dim]);
	prod += 1.0;
	return prod * prod;
}

template<t_uint DIM, typename RealT, t_uint... v>
RealT speedFpo3(t_uint idx, const RegGrid<DIM, RealT, v...>& reg_grid)
{
	assert(DIM == 3 && "Fpo3 only works in 3D");

	auto coord = reg_grid.indexToCoord(idx);
	t_uint x = coord[0];
	t_uint y = coord[1];
	t_uint z = coord[2];
	t_uint x_size_ = reg_grid.vertices_per_dir(0);
	t_uint y_size_ = reg_grid.vertices_per_dir(1);
	t_uint z_size_ = reg_grid.vertices_per_dir(2);

	t_uint noB = 11;
	if(((noB*x)/x_size_)%2 == 0)
	{
		if((((noB*y)/y_size_)%2 == 0) && (((noB*z)/z_size_)%2 == 0))
			return 2.0;
		if((((noB*y)/x_size_)%2 == 1) && (((noB*z)/x_size_)%2 == 1))
			return 2.0;
	}
	else
	{ // here (((0+noB*i)%nx)%2 == 1)
		if((((noB*y)/y_size_)%2 == 1) && (((noB*z)/z_size_)%2 == 0))
			return 2.0;
		if((((noB*y)/y_size_)%2 == 0) && (((noB*z)/z_size_)%2 == 1))
			return 2.0;
	}

	return 1.0;
}

template<t_uint DIM, typename RealT, t_uint... v>
inline RealT speedFpo4(t_uint idx, const RegGrid<DIM, RealT, v...>& reg_grid)
{
	assert(DIM == 3 && "Fpo4 only works in 3D");

	auto coord = reg_grid.indexToCoord(idx);
	t_int x = coord[0];
	t_int z = coord[2];
	t_int x_size_ = reg_grid.vertices_per_dir(0);
	t_int z_size_ = reg_grid.vertices_per_dir(2);
	t_int n_squared_ = reg_grid.vertices_per_dir(0) * reg_grid.vertices_per_dir(1); // NOT SURE !

	const RealT vel = 0.0; // impermeable
	//    const RealT vel = 0.01; // semi-permeable

	t_int noB=5;//number of barriers
	if((z*(noB+1)) % (z_size_-1) == 0)
	{ // on  a layer with obstacles
		if((std::abs(x-x_size_/2) < noB*(n_squared_-1)/(noB+1)) )
			return vel;
	}
	return 1.0;
}

template<t_uint DIM, typename RealT, t_uint... v>
RealT speedFpo6(t_uint idx, const RegGrid<DIM, RealT, v...>& reg_grid)
{
	assert(DIM == 3 && "Fpo6 only works in 3D");
	auto coord = reg_grid.indexToCoord(idx);
	t_int x = coord[0];
	t_int y = coord[1];
	t_int z = coord[2];
	t_int x_size_ = reg_grid.vertices_per_dir(0);
	t_int y_size_ = reg_grid.vertices_per_dir(1);
	t_int z_size_ = reg_grid.vertices_per_dir(2);

	t_int noB=6;//number of barriers
	//    const RealT vel = 0.0; // impermeable
	const RealT vel = 0.1 * reg_grid.stride()[0]; // semi-permeable

	if((z % (2* ( (z_size_-1)/(noB+1) ))) == 0) // on  a layer with obstacles
	{
		if(x>=(x_size_-1)/(noB+1) || y>=(y_size_-1)/(noB+1) )
			return vel;
	}
	else if((z % ((z_size_-1)/(noB+1)) ) == 0) // on  a layer with obstacles
	{
		if(x<=(noB*(x_size_-1))/(noB+1) || y<=(noB*(y_size_-1))/(noB+1) )
			return vel;
	}

	return 1.0;
}




}



 #endif  // SPEED_H_

