/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef EUCLIDIAN_DISTANCE_H_
#define EUCLIDIAN_DISTANCE_H_

#ifdef _OPENMP
#include <omp.h>
#endif

#include "solver/problem.h"
#include "grid/numerical.h"

namespace method
{

template<t_uint DIM, typename RealT, t_uint... v>
RealT closestDistance(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	RealT dist_min = Constant<RealT>::k_infinity;

	for (auto el : problem.init_list())
	{
		RealT dist = distanceEuclidian(problem.indexToReal(idx), problem.indexToReal(el)); // slow... precompute that TODO
		if (dist < dist_min)
			dist_min = dist;
	}
	return dist_min;
}

template<t_uint DIM, typename RealT, t_uint... v>
t_uint mapEuclidian(Problem<DIM, RealT, v...>& problem)
{
	for(size_t i = 0; i < problem.num_vertices(); i ++)
		problem[i] = closestDistance(i, problem);

	return 0;
}

//! Compute L_1, L_2, L_infinite errors
/*
	Produce a "error.out" file.
	Check http://www.netlib.org/lapack/lug/node75.html.
*/
template<t_uint DIM, typename RealT, t_uint... v>
void computeErrorsEuclidian(const std::string& label, const Problem<DIM, RealT, v...>& problem)
{
	RealT L1_abs_error     = 0.0;
	RealT L1_exact_error   = 0.0;
	RealT L2_abs_error     = 0.0;
	RealT L2_exact_error   = 0.0;
	RealT Linf_abs_error   = 0.0;
	RealT Linf_exact_error = 0.0;

	for(size_t idx = 0; idx < problem.num_vertices(); idx ++)
	{
		RealT exact_dist = closestDistance(idx, problem);
		RealT abs_error  = fabs(problem[idx] - exact_dist);
		// if (idx == 994517)
		// {
		// 	// 500500 center idx
		// 	std::cout << exact_dist << " " << problem[idx];
		// 	std::cout << std::endl;
		// }
		L1_abs_error    += abs_error;
		L1_exact_error  += exact_dist;
		L2_abs_error    += abs_error * abs_error;
		L2_exact_error  += exact_dist * exact_dist;

		// Useful for printing weird vertices.
		// Comment/uncomment if needed
		if (abs_error > Linf_abs_error)
		{
			printf("idx:%lu \t   exact=%lf  \t    compute=%lf  \t  abs_err=%lf\n",
				idx, exact_dist, problem[idx], abs_error);
		}

		if (abs_error > Linf_abs_error && !std::isinf(abs_error)) // Some nodes have infinite
		{
		  // printf("MAX idx:%u  err=%lf  inf err = %lf\n", idx, error, Linf_error);
			Linf_abs_error = abs_error;
		}
		if (exact_dist > Linf_exact_error)
		{
			Linf_exact_error = exact_dist;
		}
	}

	char ptr[255];
	sprintf(ptr, "Errors_%s.out",label.c_str());
	FILE *pfile = fopen(ptr, "a"); // append
	fprintf(pfile, "size=%u\n", problem.num_vertices());
	fprintf(pfile, "L1 abs error = %lf\nL1 rel err = %lf\n", L1_abs_error, L1_abs_error / L1_exact_error);
	fprintf(pfile, "L2 abs error = %lf\nL2 rel err = %lf\n", L2_abs_error, L2_abs_error / L2_exact_error);
	fprintf(pfile, "Linf abs error = %lf\nLinf rel err = %lf\n", Linf_abs_error, Linf_abs_error / Linf_exact_error);
	fclose(pfile);
}


} // end namespace method

#endif // EUCLIDIAN_DISTANCE_H_
