/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef FAST_ITERATIVE_H_
#define FAST_ITERATIVE_H_

#ifdef _OPENMP
#include <omp.h>
#endif

#include "solver/problem.h"
#include "solver/local_scheme/solve_local.h"

namespace method
{

using std::swap; // faster. Do not use std::swap() directly

// NO this would not work !
template<t_uint DIM, typename RealT, t_uint... v>
std::vector<t_uint> init_fim_deprecated_and_bad(const Problem<DIM, RealT, v...>& problem)
{
	std::vector<t_uint> aL; // active list
	for (auto el : problem.init_list())
	{
		std::vector<t_int> neighbors = problem.firstOrderPointStencil(el);
		aL.insert(aL.end(), neighbors.begin(), neighbors.end() );      // concatenate aL
	}
	aL.erase(std::remove(aL.begin(), aL.end(), -1), aL.end());  // remove -1 non neighbour
	return aL;
}

template<t_uint DIM, typename RealT, t_uint... v>
std::vector<t_uint> init_fim(const Problem<DIM, RealT, v...>& problem)
{
	std::vector<t_uint> aL; // active list
	for (auto el : problem.init_list())
	{
		std::vector<t_int> neighbors = problem.firstOrderPointStencil(el);
		for (auto x_n : neighbors)
		{
			if (x_n >= 0 && problem.u()[x_n] == 0.0) // checking if neighbor exists and is source
				aL.push_back(x_n);
		}
	}
	return aL;
}

template<t_uint DIM, typename RealT, t_uint... v>
t_uint FastIterative(Problem<DIM, RealT, v...>& problem)
{
	// initialization
	std::vector<t_uint> aL = init_fim(problem);
	removeDuplicate(aL);  // TODO : change this and use tag system

	// std::cout << "aL=[" <<  aL.size() << "]=" << printContainer(aL) << std::endl;

	t_uint it = 0;
	std::vector<RealT>  new_u(problem.u());

	while (!aL.empty())
	{
		it ++;
		size_t size = aL.size();
		std::vector<t_uint> new_aL;

		#pragma omp parallel for
		for (size_t i = 0; i < size; ++ i)
		{
			int x = aL[i];
			assert(x >= 0 && "x is equal to -1 this should not happen !");
			RealT p_outer = problem.u()[x];
			RealT q_outer = scheme::eikonalDefault(x, problem);
			// std::cout << "x=" << x << " p_out=" << p_outer << "\tq_out=" << q_outer << "\tnew_u[x]=" << new_u[x] << std::endl;

			if (fabs(p_outer - q_outer) < Constant<RealT>::k_epsilon)
			{
				new_u[x] = std::min(q_outer, problem.u()[x]);
				std::vector<t_int> neighbors = problem.firstOrderPointStencil(x); // allocate before for optimization ?
				for (auto x_n : neighbors)
				{
					if (x_n >= 0) // not -1 (i.e. neighbor do exist)
					{
						RealT p_inner = new_u[x_n];
						RealT q_inner = scheme::eikonalDefault(x_n, problem);
						if(p_inner > q_inner)
						{
							new_u[x_n] = std::min(q_inner,new_u[x_n]);
							new_aL.push_back(x_n);
						}
					}
				}
			} // endif (p_outer - q_outer) < eps
			else
			{
				if (q_outer < new_u[x])
				{
					new_aL.push_back(x);
				}
				new_u[x] = q_outer;
			}
		} // endfor for aL
		removeDuplicate(new_aL);  // TODO : change this and use tag system
		swap(aL,new_aL);
		swap(problem.u(),new_u);
		// std::cout << "aL=[" <<  aL.size() << "]=" << printContainer(aL) << std::endl;
	} // end while

	return it;
}

} // end namespace

#endif // FAST_ITERATIVE_H_

