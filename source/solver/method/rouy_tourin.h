/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef ROUY_TOURIN_H_
#define ROUY_TOURIN_H_

#ifdef _OPENMP
#include <omp.h>
#endif

#include "solver/problem.h"

using std::swap;

namespace method
{

template<t_uint DIM, typename RealT, t_uint... v>
t_uint RouyTourin(Problem<DIM, RealT, v...>& problem)
{
	std::vector<RealT> new_val(problem.num_vertices());
	bool has_converged = false;
	t_uint it = 0;
	while(!has_converged)
	{
		it ++;
		has_converged = true;
		#pragma omp parallel for
		for (t_uint i = 0; i < problem.num_vertices(); i ++)
		{
			RealT p = problem[i];
			RealT q = scheme::eikonalDefault(i,problem);
			if (q < p)
			{
				has_converged = false;
				new_val[i] = q;
			}
			else
			{
				new_val[i] = p;
			}
		} // end for
		swap(problem.u(), new_val);

		// waitKey();
	}

	return it;
}




}  // end namespace

#endif // FAST_ITERATIVE_H_

