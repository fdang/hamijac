/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef FAST_MARCHING_H_
#define FAST_MARCHING_H_


#include <cstdint>
#ifdef _OPENMP
#include <omp.h>
#endif

// #include <queue>

#include "solver/problem.h"
#include "solver/local_scheme/solve_local.h"

// Fibonacci heap is slower for the moment...
// #define USE_FIBONACCI_HEAP // comment by default

#ifdef  USE_FIBONACCI_HEAP
#include "structure/fibonacci_heap.h" // has O(1) decrease key but harder to implement and use
#else
#include "structure/binary_heap.h"
#endif

namespace method
{

using std::swap; // faster. Do not use std::swap() directly

// typedef std::priority_queue<t_uint, std::vector<t_uint>, std::greater<t_uint>> MinHeap;

// use alias template C++11
#ifdef  USE_FIBONACCI_HEAP
template<class T>
using Heap = FibonacciHeap<T>;
#else
template<class T>
using Heap = BHeap<T>;
#endif


//! Sethian's regions : Far away nodes, Narrow Band nodes, Frozen nodes
enum Regions { FA = -1, NB = 0, FZ = 1 };

template<typename RealT>
struct Point
{
	Point() : idx_(), value_() {}
	// Point(const t_uint& idx, const RealT*& u) : idx_ (idx), value_(u[idx]) {}
	Point(const t_uint& idx, const RealT& val) : idx_ (idx), value_(val) {}

	t_uint idx_;    // index in the grid
	RealT  value_;  // key for the min heap (u value function)

	void print() { std::cout << "[" << idx_ << ":" << value_ << "],"; }

	bool operator>(const Point& other) const  // for cmp=std::greater
	{
		return value_ > other.value_;
	}
	bool operator<(const Point& other) const
	{
		return value_ < other.value_;
	}
	bool operator==(const Point & rhs) const
	{
		return idx_ == rhs.idx_;
	}
	bool operator!=(const Point & rhs) const // for changeKey
	{
		return idx_ != rhs.idx_;
	}
};



template<t_uint DIM, typename RealT, t_uint... v>
void updateNB(const t_uint idx, std::vector<t_int> tag, Heap<Point<RealT>>& nB, Problem<DIM, RealT, v...>& problem)
{
	RealT res = scheme::eikonalDefault(idx, problem);
	if (tag[idx] == Regions::FA)
	{
		tag[idx] = Regions::NB;
		nB.insert(Point<RealT>(idx,res));
	}
	else
	{
		#ifdef USE_FIBONACCI_HEAP
		nB.changeKey(nB.find(Point<RealT>(idx,problem.u()[idx])), Point<RealT>(idx, res));
		#else
		nB.changeKey(Point<RealT>(idx,problem.u()[idx]), Point<RealT>(idx, res));
		#endif
	}
	if (res < problem.u()[idx])
		problem.u()[idx] = res;
}

template<t_uint DIM, typename RealT, t_uint... v>
Heap<Point<RealT>> initFMM(std::vector<t_int> tag, Problem<DIM, RealT, v...>& problem)
{
	// Narrow band as min heap !
	// Heap<Point> nB(problem.init_list()); // this cannot work directly need to have vector<Point>
	Heap<Point<RealT>> nB;
	for (auto el : problem.init_list())
	{
		nB.insert(Point<RealT>(el,problem[el]));
		tag[el] = Regions::FZ;
		// u(x) = 0 is already done in frontInitialize();
		std::vector<t_int> neighbors = problem.fivePointStencil(el);
		for (auto x_n : neighbors)
		{
			if (x_n >= 0 && problem[x_n] == 0.0) // checking if neighbor exists and is source
			{
				updateNB(x_n, tag, nB, problem);
			}
		}
	}
	return nB;
}

template<t_uint DIM, typename RealT, t_uint... v>
t_uint FastMarching(Problem<DIM, RealT, v...>& problem)
{
	// initialization
	std::vector<t_int> tag(problem.num_vertices(), Regions::FA);
	Heap<Point<RealT>> nB = initFMM(tag, problem);

	// std::cout << "nB=[" <<  nB.size() << "]={";
	// for (auto el : nB) el.print(); std::cout << "}" << std::endl;

	t_uint it = 0;
	while (!nB.empty())
	{
		it ++;
		Point<RealT> xmin = nB.extract();
		nB.pop();

		problem.u()[xmin.idx_] = xmin.value_;
		tag[xmin.idx_] = Regions::FZ;

		std::vector<t_int> neighbors = problem.fivePointStencil(xmin.idx_); // allocate before for optimization ?
		for (auto x_n : neighbors)
		{
			if (x_n >= 0 && tag[x_n] != Regions::FZ) // not -1 (i.e. neighbor do exist)
			{
				updateNB(x_n, tag, nB, problem);
			}
		}
		if (it % 1000 == 0) { saveVTK(it, "FMM", problem); std::cout <<  "NB size="<< nB.size(); }
	} // end while

	return it;
}

} // end namespace

#endif // FAST_MARCHING_H_

