/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef SEMI_ORDERED_FAST_ITERATIVE_H_
#define SEMI_ORDERED_FAST_ITERATIVE_H_

#ifdef _OPENMP
#include <omp.h>
#endif

#include "solver/problem.h"
#include "solver/local_scheme/solve_local.h"

namespace method
{

//! SOFI method
/*! SOFI method from Tor.
	Florian version.
*/
template<t_uint DIM, typename RealT, t_uint... v>
t_uint SemiOrderedFastIterative(Problem<DIM, RealT, v...>& problem)
{
	// std::vector<t_uint> aL = init_fim(problem);  // active list
	std::vector<t_uint> aL = problem.init_list(); // active list is made by init points front at beginning

	// std::cout << "aL=[" <<  aL.size() << "]=" << printContainer(aL) << std::endl;
	std::vector<t_uint> pL; // pause list

	t_uint it = 0;  // iteration

	RealT av = 0.0;
	RealT mk_average = 0.0; // this is current iteration k
	std::vector<RealT> mk;  // this is for iteration k-1

	t_uint aL_counter = 0;
	t_uint pL_counter = 0;

		RealT coeff = 1.15;
	if (DIM == 3)
		coeff = 1.10;  // this seems to depend on grid size to get 75%

	std::vector<RealT>  new_u(problem.u());

	while (!aL.empty())
	{
		it ++;
		size_t size = aL.size();
		std::vector<t_uint> new_aL;

		 // #pragma omp parallel for
		for (size_t i = 0; i < size; ++ i)
		{
			int x = aL[i]; assert(x >= 0 && "x is equal to -1 this should not happen !");
			std::vector<t_int> neighbors = problem.firstOrderPointStencil(x); // allocate before for optimization ?
			for (auto x_n : neighbors)
			{
				if (x_n >= 0)
				{
					assert(x_n >= 0 && "x is equal to -1 this should not happen !");
					RealT t_new = scheme::eikonalDefault(x_n, problem);
					if (t_new < problem.u()[x_n] && t_new > av) // not -1 (i.e. neighbor do exist)
					{
						#pragma omp critical
						{
							pL_counter ++;
							pL.push_back(x_n);
							mk.push_back(t_new); // adding the new solution value to mk
							// mk.push_back(problem.u()[x_n]); // adding the old solution value to mk
						}
					}
					else if (t_new < problem.u()[x_n] && t_new <= av)
					{
						#pragma omp critical
						{
						aL_counter ++;
						new_aL.push_back(x_n);
						// aL.push_back(x_n); // heap-use-after-free error
						// pL.push_back(x_n); // back to FIM method...
						// x_iter = aL.end();
						}
					}
					problem.u()[x_n] = std::min(problem.u()[x_n], t_new);
				}
			} // end for x_n
		} // end for aL_

		aL.swap(new_aL);
		removeDuplicate(new_aL);  // change this and use tag system !
		// problem.u().swap(new_val);

		if (aL.empty())
		{
			aL.swap(pL);
			pL.clear();     // not useful ?

			av = coeff * mk_average;
			if(!mk.empty())
			{
				mk_average = std::accumulate(mk.begin(), mk.end(), 0.0) / mk.size(); // computing mean
				mk.clear();
			}
			else
				mk_average = 0.0;

			// 	std::cout << "swapping ";
			// 	for (auto m : mk) std::cout << m << ","; std::cout << "mk_average =" << mk_average << std::endl;
		}
	} // end while

	std::cout << "(i) SOFIM : aL counter " << aL_counter << "(" << (RealT(aL_counter)/RealT(problem.num_vertices()))*100. << "%) vs pL counter "
	<< pL_counter << "(" << (RealT(pL_counter)/RealT(problem.num_vertices()))*100. << "%)" << std::endl;
	return it;
}


//! SOFI method
/*! SOFI method from Tor.
	Josef version.
*/
template<t_uint DIM, typename RealT, t_uint... v>
t_uint SofiOptimized(Problem<DIM, RealT, v...>& problem)
{
	std::vector<t_uint> aL = problem.init_list(); // active list is made by init points front at beginning
	std::vector<t_uint> pL; // pause list

#if 0
	t_uint it = 0;  // iteration

	std::vector<RealT>  new_u(problem.u());

    RealT av = 0.0;
    t_uint aL_loop_counter = 0;
    t_uint pL_loop_counter = 0;
    t_uint aL_swaps_counter = 0;
    RealT mk_average = 0.0;
    RealT mk_prev = 0.0;
    RealT mk_relax = 0.0;
    RealT sigma = 0;
    RealT coeff = 1.0;

    #ifdef _OPENMP
	int omp_max_threads = omp_get_max_threads();
	std::vector< t_uint* > new_aL_threads(omp_max_threads);
	#pragma omp parallel for schedule(static,1)
	for(int i = 0; i < omp_max_threads; i++)
	{
		new_aL_threads[i] = new t_uint[node_size()];
		new_aL_threads[i][0] = 0;
	}

	std::vector< t_uint* > pL_threads(omp_max_threads);
	#pragma omp parallel for schedule(static,1)
	for(int i = 0; i < omp_max_threads; i++)
	{
		pL_threads[i] = new t_uint[node_size()];
		pL_threads[i][0] = 0;
	}

	std::vector<RealT>  tsum_threads(omp_max_threads, 0.0);
	std::vector<RealT>  tsqsum_threads(omp_max_threads, 0.0);
	#endif

	// to complete

#endif
	return 0;
}

} // end namespace method





#endif // SEMI_ORDERED_FAST_ITERATIVE_H_

