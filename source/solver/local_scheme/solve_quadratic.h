/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef SOLVE_QUADRATIC_H_
#define SOLVE_QUADRATIC_H_

#include "tools/typedefs.h"

namespace quadratic 
{

template<typename RealT>
static const RealT quadraticFlorian(RealT a, RealT b_prime, RealT c)
{
	RealT delta_prime = b_prime * b_prime - a * c;
	if (delta_prime < Constant<>::k_epsilon)
		return Constant<>::k_infinity;

	return (b_prime + sqrt(delta_prime)) / a;
}


// first order test
template<typename RealT>
RealT solveFirstOrder(RealT umin_sub, RealT umin_sup, RealT h, RealT f){
    if (fabs(umin_sub - umin_sup) >= std::sqrt(2.0) * h/f)
        return std::min(umin_sub, umin_sup) + h/f;
    else
        return (umin_sub + umin_sup + std::sqrt(2*h*h/(f*f)-(umin_sub-umin_sup)*(umin_sub-umin_sup)))/2.0;
}

// second order test
template<typename RealT>
RealT solveSecondOrder(RealT umin_sub, RealT umin_sup, RealT h, RealT f)
{
    RealT fact = 9/(4.0*h*h);
    RealT a = 0.0, b = 0.0, c = 0.0;
    
    a += (umin_sub != 0.0) ? 1/h : 0;
    a += (umin_sup != 0.0) ? 1/h : 0;
    a *= fact;
    b += umin_sub/(h*h) + umin_sup/(h*h);
    b *= - fact;
    c += umin_sub*umin_sub/(h*h) + umin_sup*umin_sup/(h*h);
    c *= fact;
    c -= 1 / (f*f);
    
    RealT delta = b*b - 4*a*c;
    if (delta < 0) 
    	return Constant<RealT>::k_infinity;
    
    return -b + std::sqrt(delta)/(2*a);
}


}

#endif // SOLVE_QUADRATIC_H_
