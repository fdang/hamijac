/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef SOLVE_LOCAL_H_
#define SOLVE_LOCAL_H_

#include <algorithm> // min, max

#include "solver/problem.h"
#include "solver/local_scheme/solve_quadratic.h"
// #include "tools/typedefs.h"


namespace scheme
{

// Need to investigate this
// simpleFirstOrderCartesian works only in 2D because of 1st order quadratic solve function
// simpleSecondOrderCartesian does not work problem with 2nd order quadratic solve function
// godunovFlorian some numerical errors
// JosefEikonal3D works only in 3D



template<t_uint DIM, typename RealT, t_uint... v>
RealT simpleFirstOrderCartesian(t_uint idx, const Problem<DIM, RealT, v...>& problem);

// Different schemes
template<t_uint DIM, typename RealT, t_uint... v>
RealT godunovFlorian(t_uint idx, const Problem<DIM, RealT, v...>& problem);

template<t_uint DIM, typename RealT, t_uint... v>
RealT solveEikonalJosef3D(t_uint idx, const Problem<DIM, RealT, v...>& problem);

template<t_uint DIM, typename RealT, t_uint... v>
RealT finiteFirstOrderCartesian(t_uint idx, const Problem<DIM, RealT, v...>& problem);

template<t_uint DIM, typename RealT, t_uint... v>
RealT finiteSecondOrderCartesian(t_uint idx, const Problem<DIM, RealT, v...>& problem);

// Default scheme used
template<t_uint DIM, typename RealT, t_uint... v>
RealT eikonalDefault(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	return problem.scheme()(idx, problem); // Not optimized TODO use template
}

// only works for 2D
template<t_uint DIM, typename RealT, t_uint... v>
RealT simpleFirstOrderCartesian(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	// RealT square_stride = problem.stride()[0] * problem.stride()[0];
	// RealT a = 0.0;
	// RealT b_prime = 0.0;
	// RealT vel = problem.vel()[idx];
	// RealT c = -1.0 / (vel * vel) ;

	std::vector<t_int> neighbors = problem.firstOrderPointStencil(idx);

	std::vector<RealT> min_dim_val(DIM, problem.u()[idx]);

	for (t_uint d = 0; d < DIM; d ++)
	{
		t_int left = neighbors[2 * d];
		t_int right = neighbors[2 * d + 1];
		if (left >= 0)
			min_dim_val[d] = std::min(problem.u()[left], min_dim_val[d]);
		if (right >= 0)
			min_dim_val[d] = std::min(problem.u()[right], min_dim_val[d]);
		
		// if (min_dim_val[d] < problem.u()[idx] && min_dim_val[d] < Constant<RealT>::k_infinity)
		// {
		// 	a += 1.0 / square_stride;
		// 	b_prime += min_dim_val[d] / square_stride;
		// 	c += min_dim_val[d] * min_dim_val[d]  / square_stride;
		// }
	}
	// return quadratic::quadraticFlorian(a, b_prime, c); // b = -2*b_prime;
	return quadratic::solveFirstOrder(min_dim_val[0], min_dim_val[1], problem.stride()[0], problem.vel()[idx]);
}

template<typename RealT>
RealT computeSecondOrder(RealT dist_1, RealT dist_2)
{
	// assert(dist_2 <= dist_1);
	// if (!std::isinf(dist_1) && !std::isinf(dist_2))
		return (4 * dist_1 - dist_2) / 3.0;
	// else
		// return std::numeric_limits<RealT>::infinity();
}

// only works for 2D
template<t_uint DIM, typename RealT, t_uint... v>
RealT simpleSecondOrderCartesian(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	std::vector<t_int> neighbors = problem.secondOrderPointStencil(idx);
	std::vector<RealT> min_dim_val(DIM, problem.u()[idx]);

	for (t_uint d = 0; d < DIM; d ++)
	{
		RealT left_1_val  = std::numeric_limits<RealT>::infinity();
		RealT right_1_val = std::numeric_limits<RealT>::infinity();
		RealT left_2_val  = std::numeric_limits<RealT>::infinity();
		RealT right_2_val = std::numeric_limits<RealT>::infinity();

		t_int left_2  = neighbors[4 * d];
		t_int left_1  = neighbors[4 * d + 1];		
		t_int right_1 = neighbors[4 * d + 2];
		t_int right_2 = neighbors[4 * d + 3];

		if (left_2 >= 0)  left_2_val   = problem.u()[left_2];
		if (left_1 >= 0)  left_1_val   = problem.u()[left_1];
		if (right_1 >= 0) right_1_val  = problem.u()[right_1];
		if (right_2 >= 0) right_2_val  = problem.u()[right_2];

		if (!std::isinf(left_2_val) && !std::isinf(left_1_val) && !std::isinf(right_1_val) && !std::isinf(right_2_val))
		{
			min_dim_val[d] = std::min({min_dim_val[d], computeSecondOrder(left_1_val, left_2_val), 
				computeSecondOrder(right_1_val, right_2_val)});
		}
		else
			min_dim_val[d] = std::min({min_dim_val[d], left_1_val, right_1_val});
	}

	if (!std::isinf(min_dim_val[0]) && !std::isinf(min_dim_val[1]) && min_dim_val[0] != 0.0)
		return std::min(problem.u()[idx],
			quadratic::solveSecondOrder(min_dim_val[0], min_dim_val[1], problem.stride()[0], problem.vel()[idx]));
	else
		return quadratic::solveFirstOrder(min_dim_val[0], min_dim_val[1], problem.stride()[0], problem.vel()[idx]);
}





template<t_uint DIM, typename RealT, t_uint... v>
RealT godunovFlorian(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	RealT a = 0.0;
	RealT b_prime = 0.0;
	RealT c = -1.0 / problem.vel()[idx];

	std::vector<t_int> neighbors = problem.firstOrderPointStencil(idx);
	std::vector<RealT> neighbors_val;

	RealT prod_stride = 1.0;

	for (t_uint d = 0; d < DIM; d ++)
	{
		RealT left = neighbors[2 * d];
		RealT right = neighbors[2 * d + 1];

		RealT d_minus = (left >= 0 ? problem.u()[left]  : Constant<RealT>::k_infinity);
		RealT d_plus = (right >= 0 ? problem.u()[right] : Constant<RealT>::k_infinity);

		RealT u_min = std::min(d_minus, d_plus);

		if (u_min < problem.u()[idx] && u_min < Constant<RealT>::k_infinity)
		{
			RealT square_stride = problem.stride()[d] * problem.stride()[d];
			prod_stride *= problem.stride()[d];
			a += 1.0 / square_stride;
			b_prime += u_min / square_stride;
			c += u_min * u_min / square_stride;
		}
	}

	// b_prime /= prod_stride;
	// c /= prod_stride;

	return quadratic::quadraticFlorian(a, b_prime, c); // b = -2*b_prime;
}




template<t_uint DIM, typename RealT, t_uint... v>
RealT solveEikonalJosef3D(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	// UintT x = idx % x_size;
	// UintT y = (idx / x_size) % y_size;
	// UintT z = idx / (x_size * y_size);
	t_uint x_size = problem.vertices_per_dir(2);
	t_uint y_size = problem.vertices_per_dir(1);
	t_uint z_size = problem.vertices_per_dir(0);
	t_uint n_squared = y_size * z_size;
	t_uint step_squared = problem.stride()[1] * problem.stride()[0];

	RealT c[3];

	if(idx % x_size == 0)
		c[0] = std::min(problem.u()[idx + 1], problem.u()[idx]);
	else
		if(idx % x_size == x_size-1)
			c[0] = std::min(problem.u()[idx - 1], problem.u()[idx]);
		else
			c[0] = std::min(problem.u()[idx - 1], problem.u()[idx + 1]);

	if (idx % n_squared < x_size)
		c[1] = std::min(problem.u()[idx+x_size], problem.u()[idx]);
	else
		if (idx % n_squared >= x_size*(y_size-1))
			c[1] = std::min(problem.u()[idx-x_size], problem.u()[idx]);
		else
			c[1] = std::min(problem.u()[idx-x_size], problem.u()[idx+x_size]);

	if (idx < n_squared)
		c[2] =  std::min(problem.u()[idx+n_squared], problem.u()[idx]);
	else
		if (idx >= n_squared*(z_size-1))
			c[2] =  std::min(problem.u()[idx-n_squared], problem.u()[idx]);
		else
			c[2] =  std::min(problem.u()[idx-n_squared], problem.u()[idx+n_squared]);

	std::sort(c, c+3);

	RealT f = problem.vel()[idx];
	RealT u = c[0] + problem.stride()[2]/f;  // This is not sure about DIM !

	if(u <= c[1]) return u;
		u = (c[1]+c[0]+sqrt(-c[1]*c[1]-c[0]*c[0]+2.0*c[1]*c[0]+2.0*(step_squared/(f*f))))/2.0;
	if(u <= c[2]) return u;
		return (2.0*(c[2]+c[1]+c[0])+sqrt(4.0*pow((c[2]+c[1]+c[0]), 2.0) - 12.0*(c[2]*c[2]+c[1]*c[1]+c[0]*c[0]-(step_squared/(f*f)))))/6.0;
}





// Below is deprecated

template<t_uint DIM, typename RealT, t_uint... v>
RealT finiteFirstOrderCartesian(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	vector<t_int> neighbors = problem.firstOrderPointStencil(idx);
	// RealT usub_min = std::numeric_limits<RealT>::infinity();
	// RealT usup_min = std::numeric_limits<RealT>::infinity();
	RealT usub_min = problem.u()[idx];
	RealT usup_min = problem.u()[idx];
#if 1
	for (t_uint d = 0; d < DIM; d ++)
	{
		RealT left_val = std::numeric_limits<RealT>::infinity();
		RealT right_val = std::numeric_limits<RealT>::infinity();
		t_int left = neighbors[2 * d];
		t_int right = neighbors[2 * d + 1];
		if (left >= 0)
			left_val  = problem.u()[left];
		if (right >= 0)
			right_val = problem.u()[right];
		if (left_val < usub_min)
			usub_min = left_val;
		if (right_val < usup_min)
			usup_min = right_val;
	}
#else
	vector<RealT> sub_differentials;
	sub_differentials.reserve(DIM);       // DIM is the max size possible so reserve it for fast push_back
	vector<RealT> sup_differentials;
	sup_differentials.reserve(DIM);       // DIM is the max size possible so reserve it for fast push_back

	for (t_uint d = 0; d < DIM; d ++)
	{
		RealT left = neighbors[2 * d];
		RealT right = neighbors[2 * d + 1];

		sub_differentials.push_back(left >= 0  ? problem.u()[left]   : std::numeric_limits<RealT>::infinity());
		sup_differentials.push_back(right >= 0 ? problem.u()[right]  : std::numeric_limits<RealT>::infinity());
	}

	usub_min = *std::min_element(sub_differentials.begin(), sub_differentials.end()); // not fast TODO
	usup_min = *std::min_element(sup_differentials.begin(), sup_differentials.end());
#endif
	// if (!std::isinf(usub_min) && !std::isinf(usup_min))
	// 	std::cout << idx << "#" << usub_min << ";" << usup_min << std::endl;

	// This only work for cartesian grid
	return quadratic::solveFirstOrder(usub_min, usup_min, problem.stride()[0], problem.vel()[idx]);
}


template<t_uint DIM, typename RealT, t_uint... v>
RealT finiteSecondOrderCartesian(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	vector<t_int> neighbors = problem.secondOrderPointStencil(idx);
	// std::cout << idx << printContainer(neighbors) << std::endl;

	RealT usub_min = std::numeric_limits<RealT>::infinity();
	RealT usup_min = std::numeric_limits<RealT>::infinity();
	RealT usub_min_2 = std::numeric_limits<RealT>::infinity();
	RealT usup_min_2 = std::numeric_limits<RealT>::infinity();

	// vector<RealT> neigh_values(4 * DIM,std::numeric_limits<RealT>::infinity());
	// for (t_uint i = 0; i < 4 * DIM; ++ i)
	// {
	// 	if (neighbors[i] >= 0)
	// 		neigh_values[i] = problem.u()[neighbors[i]];
	// }
	// std::cout << idx << printContainer(neigh_values) << std::endl;
#if 1
	for(t_uint d = 0; d < DIM; d ++)
	{
		RealT left_1_val = std::numeric_limits<RealT>::infinity();
		RealT right_1_val = std::numeric_limits<RealT>::infinity();
		RealT left_2_val = std::numeric_limits<RealT>::infinity();
		RealT right_2_val = std::numeric_limits<RealT>::infinity();

		t_int left_2  = neighbors[4 * d];
		t_int left_1  = neighbors[4 * d + 1];		
		t_int right_1 = neighbors[4 * d + 2];
		t_int right_2 = neighbors[4 * d + 3];

		if (left_2 >= 0)  left_2_val   = problem.u()[left_2];
		if (left_1 >= 0)  left_1_val   = problem.u()[left_1];
		if (right_1 >= 0) right_1_val  = problem.u()[right_1];
		if (right_2 >= 0) right_2_val  = problem.u()[right_2];

		if (left_1_val < usub_min)
			usub_min = left_1_val;
		if (computeSecondOrder(left_1_val, left_2_val) < usub_min_2)
			usub_min_2 = computeSecondOrder(left_1_val, left_2_val);
		if (right_1_val < usup_min)
			usup_min = right_1_val;
		if (computeSecondOrder(right_1_val, right_2_val) < usup_min_2)
			usup_min_2 = computeSecondOrder(right_1_val, right_2_val);
	}
#else
	vector<RealT> sub_differentials;
	sub_differentials.reserve(2 * DIM); // 2 * DIM is the max size possible so reserve it for fast push_back
	vector<RealT> sup_differentials;
	sup_differentials.reserve(2 * DIM); // 2 * DIM is the max size possible so reserve it for fast push_back
	for (t_uint d = 0; d < DIM; d ++)
	{
		RealT left  = neighbors[4 * d + 1];
		RealT right = neighbors[4 * d + 2];

		// if (left >= 0)
		// 	sub_differentials.push_back(problem.u()[left]);
		// if (right >= 0)
		// 	sub_differentials.push_back(problem.u()[right]);		
		
		sub_differentials.push_back(left  >= 0 ? problem.u()[left]   : std::numeric_limits<RealT>::infinity());
		sup_differentials.push_back(right >= 0 ? problem.u()[right]  : std::numeric_limits<RealT>::infinity());

		RealT left2  = neighbors[4 * d];
		RealT right2 = neighbors[4 * d + 3];

		// if (left2 >= 0)
		// 	sub_differentials.push_back(problem.u()[left2]);
		// if (right2 >= 0)
		// 	sub_differentials.push_back(problem.u()[right2]);	

		sub_differentials.push_back(left2  >= 0 ? problem.u()[left2]   : std::numeric_limits<RealT>::infinity());
		sup_differentials.push_back(right2 >= 0 ? problem.u()[right2]  : std::numeric_limits<RealT>::infinity());
	}

	usub_min = *std::min_element(sub_differentials.begin(), sub_differentials.end()); // not fast TODO
	usup_min = *std::min_element(sup_differentials.begin(), sup_differentials.end());
#endif
	// if (!std::isinf(usub_min) || !std::isinf(usup_min))
	// 	std::cout << idx << "#" << usub_min << ";" << usup_min  <<  " => " << quadratic::solveSecondOrder(usub_min, usup_min, problem.stride()[0], problem.vel()[idx]) << std::endl;

	// This only work for cartesian
	if (!std::isinf(usub_min_2) && !std::isinf(usup_min_2))
	{
		// std::cout << idx << "#" << usub_min << ";" << usup_min  <<  " => " << quadratic::solveSecondOrder(usub_min, usup_min, problem.stride()[0], problem.vel()[idx]) << std::endl;
		return quadratic::solveSecondOrder(usub_min_2, usup_min_2, problem.stride()[0], problem.vel()[idx]);
	}
	else
	{
		return quadratic::solveFirstOrder(usub_min, usup_min, problem.stride()[0], problem.vel()[idx]);
	}
}


#if 0

template<t_uint DIM, typename RealT, t_uint... v>
RealT GenericOrderCartesian(t_uint idx, const Problem<DIM, RealT, v...>& problem)
{
	std::vector<RealT> min_dim_val(DIM, std::numeric_limits<RealT>::infinity());
	vector<t_int> neighbors = problem.firstOrderPointStencil(idx);
	t_uint fd_order = neighbors.size() / 2;    // finite difference order

	for (t_uint d = 0; d < DIM; d ++)
	{
		for (t_uint i = 0; i < fd_order; i ++) // on one dimension
		{
			t_int neighbor = neighbors[fd_order * d + i];
			if (neighbor >= 0)  // if neighbor exists
			{
				RealT val = problem.u()[neighbor];
				if (val < min_dim_val[d])
					min_dim_val[d] = val;
			}
		}
	}
	RealT vel = problem.vel()[idx];
	// if (vel > 0)
	{
		RealT a = min_dim_val[0];
		RealT b = min_dim_val[1];
		if(std::fabs(a-b) >= vel) 
			return std::min(a,b) + vel;
		else 
			return a + b + std::sqrt(-a*a -b*b + 2.0 * a * b + 2.0 * (vel * vel)) / 2.0;
	}
}

#endif




} // end namespace

#endif // SOLVE_LOCAL_H_


