/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef GEOMETRY_FRONT_
#define GEOMETRY_FRONT_

#include <random>
#include <set>

namespace front
{

template<t_uint DIM, typename RealT, t_uint... v>
std::vector<t_uint> randomSeedPoint(const t_uint num_seeds, const RegGrid<DIM, RealT, v...>& reg_grid)
{
	// t_uint seed_val;          // uncomment for seed random
	std::mt19937 rng;            // Mersenne Twister keep one global instance (per thread)
	
	// rng.seed(seed_val);

	std::uniform_int_distribution<t_uint> rand_uint(0, reg_grid.num_vertices());
	std::set<t_uint> set_init_points;
	for(t_uint i = 0; i < num_seeds; i ++)
		set_init_points.insert(rand_uint(rng));
	// copy to vector
	return std::vector<t_uint>(set_init_points.begin(), set_init_points.end());
}

}

#endif // GEOMETRY_FRONT_


