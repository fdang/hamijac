/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef PROBLEM_H_
#define PROBLEM_H_

#include <initializer_list>
using std::initializer_list;

#include "grid/regular_grid.h"

// alias declaration for ArrRealT
// template <typename RealT=double>
// using ArrRealT = vector<RealT>;

// typedef vector<RealT>    ArrRealT;

template<t_uint DIM, typename RealT, t_uint... v>
class Problem
{
public:
	// for iterators // not needed now
	// typedef std::ptrdiff_t   difference_type;
	// typedef std::size_t      size_type;
	// typedef RealT            value_type;
	// typedef RealT*           pointer;
	// typedef RealT&           reference;

	explicit Problem(const RegGrid<DIM, RealT, v...>& grid) :
		grid_(grid),
		u_(grid.num_vertices(), std::numeric_limits<RealT>::infinity()), // Constant<RealT>::k_infinity
		vel_(grid.num_vertices(), 1.0),
		init_list_(),
		method_(nullptr),
		scheme_(nullptr) {}

	// Access operators for u value function
	      RealT& operator[] (const size_t idx)       { return u_[idx]; }
	const RealT& operator[] (const size_t idx) const { return u_[idx]; }
	const RealT& at(vector<t_uint> coord) const
	{
		if(coord.size() != DIM)
			throw std::invalid_argument("at() value function does not have good coordinates size !");
		return u_[grid_.coordToIndex(coord)];
	}
	RealT& at(vector<t_uint> coord)
	{  
		if(coord.size() != DIM)
			throw std::invalid_argument("at() value function does not have good coordinates size !");
		return u_[grid_.coordToIndex(coord)];
	}

	// pointer begin()       { return u_.begin(); }
	// pointer begin() const { return u_.begin(); }
	// pointer end()         { return u_.end(); }
	// pointer end()   const { return u_.end(); }

	vector<RealT>& u() { return u_; } // used for methods. u_ is mutable here !

	const vector<t_uint>& init_list() const { return init_list_; }  // used for methods
	const vector<RealT>&  u()         const { return u_;         } // used for methods
	const vector<RealT>&  vel()       const { return vel_;       } // used for solveEikonal

	const vector<RealT>&  stride()    const { return grid_.stride(); } // used in solveEikonal
	t_uint num_vertices()             const { return grid_.num_vertices(); }
	t_uint vertices_per_dir(t_uint dim) const { return grid_.vertices_per_dir(dim); } // used for saveVTK

	const vector<t_int> firstOrderPointStencil(const t_uint& idx) const { return grid_.firstOrderPointStencil(idx); }
	const vector<t_int> secondOrderPointStencil(t_uint idx) const { return grid_.secondOrderPointStencil(idx); }

	const vector<RealT> giveRealOneDim(const t_uint& dim)   const { return grid_.giveRealOneDim(dim); }  // used for saveVTK
	const vector<RealT> indexToReal(const t_uint& idx)      const { return grid_.indexToReal(idx); } // for euclidian map
	t_uint realToIndex(const vector<RealT>& real)           const { return grid_.realToIndex(real); } // for path finding
	t_uint coordToIndex(const vector<t_uint>& coord)        const { return grid_.coordToIndex(coord); }// gradient descent
	const RegGrid<DIM, RealT, v...>& grid() const { return grid_; }  // used for gradient descent (and saveVTK ?)

	const std::function<RealT(t_uint, const Problem<DIM, RealT, v...>&)>& scheme() const { return scheme_; }
	// std::function<RealT(t_uint, Problem<DIM, RealT, v...>&)> scheme() { return scheme_; }

	void print() const
	{
		grid_.print();
		std::cout << "u(x)="   << printContainer(u_) << std::endl;
		std::cout << "vel(x)=" << printContainer(vel_) << std::endl;
		std::cout << "init[" << init_list_.size() << "]=" << printContainer(init_list_) << std::endl;
	}

	t_int frontInitialize(vector<t_uint> front_vertices)
	{
		for (auto el : front_vertices)
			u_[el] = static_cast<RealT>(0.0);

		init_list_.insert(init_list_.end(), front_vertices.begin(), front_vertices.end());
		return init_list_.size();
	}

	t_int frontInitialize(initializer_list<vector<t_uint>> list_vec)
	{
		t_uint sum = 0;
		for (auto el : list_vec)
			sum += frontInitialize(el);
		return sum;
	}

	t_int frontInitialize()
	{
		if (init_list_.empty())
		{
			std::cout << "Warning ! Initialization list is empty !" << std::endl;
			return -1;
		}
		for (auto el : init_list_)
			u_[el] = static_cast<RealT>(0.0);
		return init_list_.size();
	}

	//! Change velocity according to a specific speed function (see speed/speed_function.h)
	/*!
	   ex : problem.setVelocity(&velocity::speedChacon)
	*/
	t_uint setVelocity(std::function<RealT(t_uint, const RegGrid<DIM, RealT, v...>&)> speed_func)
	{
		t_uint count = 0;
		std::cout << "# setting velocity map... ";
		#pragma omp parallel for
		for (size_t idx = 0; idx < num_vertices(); idx ++)
		{
			vel_[idx] = speed_func(idx, grid_);
			if (vel_[idx] != static_cast<RealT>(1.0))
				count ++;
		}
		std::cout << "[done]" << std::endl;
		return count;
	}

	//! Change velocity with a given value on given vertices
	/*!
	   ex : auto circle = createBall(grid);
			auto vel_front_circle = circle({-0.0, 0.0}, 0.4);
			problem.setVelocity(2.0,vel_front_circle);  // points inside circle are 2x faster
	*/
	t_uint setVelocity(RealT value, vector<t_uint> vel_vertices)
	{
		for (auto el : vel_vertices)
			vel_[el] = static_cast<RealT>(value);
		return vel_vertices.size();
	}

	t_uint setVelocity(RealT value, std::initializer_list<vector<t_uint>> list_vec)
	{
		t_uint sum = 0;
		for (auto el : list_vec)
			sum += setVelocity(value, el);
		return sum;
	}

	void setVelocity(std::vector<RealT> vel)
	{
		assert(vel.size() == vel_.size());        // TODO except
		for (t_uint i = 0; i < vel.size(); i ++)
			vel_[i] = vel[i];
	}

	t_uint setObstacle(vector<t_uint> vel_vertices) { return setVelocity(0.0, vel_vertices); }
	t_uint setObstacle(std::initializer_list<vector<t_uint>> list_vec) { return setVelocity(0.0, list_vec); }

	void setHJBMethod(std::function<t_uint(Problem<DIM, RealT, v...>&)> method) { method_ = method; }
	void setHJBScheme(std::function<RealT(t_uint, const Problem<DIM, RealT, v...>&)> scheme) { scheme_ = scheme; }

	t_int solveHJB(std::function<t_uint(Problem<DIM, RealT, v...>&)> method,
		std::function<RealT(t_uint, Problem<DIM, RealT, v...>&)> scheme)
	{
		setHJBScheme(scheme);
		setHJBMethod(method);
		return solveHJB();
	}
	t_int solveHJB(std::function<t_uint(Problem<DIM, RealT, v...>&)> method)
	{
		setHJBMethod(method);
		return solveHJB();
	}

	//! Call Rouy-Tourin, FIM, SOFI methods to solve the HJB problem
	/*! It is recommended to use solveHJB() over calling method directly in order to reinitialize u_ value function
		initial fronts for different simulations
	*/
	t_int solveHJB()
	{
		if (method_)
		{
			std::cout << ">> Solving HJB problem...\n";
			frontInitialize();      // reinitialize u_ value function and initial fronts for next simulation
			if(scheme_)
				return method_(*this);
			else
			{
				std::cerr << "Error ! Scheme for solving HJB equation is not defined." << std::endl;
				return -1;
			}
		}
		else
		{
			std::cerr << "Error ! Method for solving HJB equation is not defined." << std::endl;
			return -1;
		}
	}

private:
	const RegGrid<DIM, RealT, v...>& grid_; //!< regular grid for the moment
	vector<RealT>  u_;          //!< value function
	// ArrRealT u_;
	vector<RealT>  vel_;        //!< initialize vel tables at beginning or compute on the fly
	vector<t_uint> init_list_;  //!< vertices in initial fronts

	std::function<t_uint(Problem<DIM, RealT, v...>&)> method_;        //!< Current registered method to use for this problem
	std::function<RealT(t_uint, const Problem<DIM, RealT, v...>&)> scheme_; //!< Scheme method
};

template<t_uint DIM, typename RealT, t_uint... v>
Problem<DIM,RealT,v...> createProblem(const RegGrid<DIM, RealT, v...>& grid)
{
	return Problem<DIM,RealT,v...>(grid);
}

#endif // PROBLEM_H_
