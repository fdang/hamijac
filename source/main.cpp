/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#include "hamijac.h"

void test_grid_0()
{
	// create a 2D grid 9x14  with lower bounds {-4.0,-3.0} and upper bounds {4.0,3.0}
	RegGrid<2, float, 9, 14> grid({-4.0,-3.0}, {4.0,3.0});
	grid.print();
	auto real_coord = grid.giveAllReal();
	std::cout << printContainer(real_coord) << std::endl;
	std::cout << "Slice 0 :" << printContainer(grid.giveRealOneDim(0)) << std::endl;
	std::cout << "Slice 1 :" << printContainer(grid.giveRealOneDim(1)) << std::endl;
	std::cout << "closest : " << printContainer(closestVertex({-0.4f,2.0f}, grid)) << std::endl;
}

void test_grid_1()
{
	RegGrid<3, float, 100> grid(-4.0, 4.0);
	// RegGrid<3, float, 100> grid(-4.0, -4.0); // this will throw an exception !
	grid.print();
}

void test_grid_2()
{
	RegGrid<4, float, 10, 15, 10, 18> grid({-4.0,-3.0,-1.0,-2.0}, {6.0,5.0,10.0,8.0});;
	grid.print();
	auto real_coord = grid.giveAllReal();
	auto neigh = grid.firstOrderPointStencil(200);
	std::cout << "Neighbors[200]:" << printContainer(neigh) << std::endl;
}

void test_grid_3()
{
	// default is 10x10 when DIM>=4
	RegGrid<5, float> grid;
	grid.print();
	auto real_coord = grid.giveAllReal();
	auto neigh = grid.firstOrderPointStencil(200);
	// std::cout << "Neighbors[200]:" << printContainer(neigh) << std::endl;
}

void test_geometry_1()
{
	RegGrid<2, float> grid;
	grid.print();
	std::cout << "Slice 0 : " << printContainer(grid.giveRealOneDim(0)) << std::endl;
	std::cout << "Slice 1 : " << printContainer(grid.giveRealOneDim(1)) << std::endl;

	// Create a point class functor for the grid
	auto point_grid = createPoint(grid);

	// Call this functor for 2 real points
	auto init_point = point_grid({{-0.6,0.2},{-0.2,0.6}});
	std::cout << "Points concerned : " << printContainer(init_point) << std::endl;

	auto circle = createBall(grid);
	auto vertices_inside_circle = circle({-0.21, 0.34}, 2.8);
	std::sort(vertices_inside_circle.begin(), vertices_inside_circle.end());
	std::cout << vertices_inside_circle.size() << " vertices inside circle : " << printContainer(vertices_inside_circle) << std::endl;
}

void test_center()
{
	// Create a 2D grid with default size 1000x1000 and default boundaries [-1;1]^2
	RegGrid<2,float, 100> grid(-10.0,10.0);

	auto point = createPoint(grid);
	auto center_point = point({0.0, 0.0});

	auto problem = createProblem(grid);
	problem.frontInitialize(center_point);
	// problem.print();

	t_uint num_iterations = 0;

	// Schemes are : godunovFlorian, finiteFirstOrderCartesian, finiteSecondOrderCartesian
	// problem.setHJBScheme(&scheme::godunovFlorian<2,float, 100>); // can be changed
	// problem.setHJBScheme(&scheme::finiteFirstOrderCartesian<2,float, 100>); // can be changed
	// problem.setHJBScheme(&scheme::finiteSecondOrderCartesian<2,float, 100>); // can be changed
	problem.setHJBScheme(&scheme::simpleFirstOrderCartesian<2,float, 100>);
	// problem.setHJBScheme(&scheme::simpleSecondOrderCartesian<2,float, 100>);

	// Methods are : FastIterative, RouyTourin, mapEuclidian, SemiOrderedFastIterative, FastMarching
	problem.setHJBMethod(&method::RouyTourin<2,float, 100>); // can be changed

	num_iterations = problem.solveHJB();
	// or
	// problem.setHJBMethod(&method::FastIterative<2, float, 100>);
	// num_iterations = problem.solveHJB();
	// or
	// num_iterations = problem.solveHJB(&method::FastIterative<2, float,100>);
	// or
	// num_iterations = problem.solveHJB(&method::RouyTourin<2, float, 100>);
	// or
	// num_iterations = problem.solveHJB(&method::FastMarching<2, float, 100>);
	// or
	// num_iterations = problem.solveHJB(&method::mapEuclidian<2, float, 100>);
	// or
	// num_iterations = problem.solveHJB(&method::SemiOrderedFastIterative<2, float, 100>);

	// or (not recommended ! because this does not reinitialize value function, velocities values)
	// method::RouyTourin(problem);

	std::cout << "Center test done in " <<  num_iterations << " iterations." << std::endl;
	saveVTK(num_iterations, "center", problem);

	// method::computeErrorsEuclidian("center", problem);
}

void test_chacon_sphere()
{
	RegGrid<3, float, 100> grid;

	auto sphere = createBall(grid);
	auto init_front_circle = sphere({0.0, 0.4, -0.4}, 0.1);

	auto problem = createProblem(grid);
	problem.frontInitialize(init_front_circle);
	problem.setVelocity(&velocity::speedChacon<3, float, 100>);
	// problem.print();  // display problem information (grid,velocity function, value function, initial fronts...)
	t_uint num_iterations = problem.solveHJB(&method::SemiOrderedFastIterative<3, float, 100>);

	std::cout << "SOFIM done in " <<  num_iterations << " iterations." << std::endl;
	saveVTK(num_iterations, "SOFI", problem);
}


void test_speed_center()
{
	RegGrid<3, float> grid;
	auto point = createPoint(grid);
	auto center_point = point({0.0, 0.0, 0.0});

	auto problem = createProblem(grid);
	problem.frontInitialize(center_point);
	problem.setVelocity(&velocity::speedFpo4<3, float>);

	// t_uint num_iterations = problem.solveHJB(&method::RouyTourin<3, float>);
	t_uint num_iterations = problem.solveHJB(&method::SemiOrderedFastIterative<3, float>);
	std::cout << "SOFIM done in " <<  num_iterations << " iterations." << std::endl;
	saveVTK(num_iterations, "SOFI", problem);
}

void test_front_obstacle()
{
	RegGrid<2, float, 1000> grid(-2.0f, 2.0f);
	auto point = createPoint(grid);
	auto polygon = createPolygon(grid);
	auto circle = createBall(grid);
	auto line = createSegment(grid);

	auto problem = createProblem(grid);

	// auto center_point = point({1.4, 1.2}); // Path 1
	auto center_point = point({1.4, 0.2});    // Path 2
	auto circle_init = circle({-1.5, -1.6}, 0.1);
	problem.frontInitialize({center_point, circle_init}); // two initial fronts

	auto polygon_1 = polygon({{0.2, 1.2}, {1.0, 0.3}, {1.1, 1.3}});  // triangle
	auto polygon_2 = polygon({{-0.4,-1.2}, {-1.0, -0.3}, {-1.4, -1.4}, {-1.6, -0.6}}); // quadrilateral
	auto circle_obs = circle({-1.2, 0.6}, 0.3);
	auto line_1 = line({{-1.2,-1.4},{1.0,0.8}}, 0.1);
	problem.setObstacle({polygon_1, polygon_2, circle_obs, line_1});

	// problem.setVelocity(&velocity::speedChacon<2, float, 1000>);
	// problem.print();

	problem.setHJBScheme(&scheme::simpleFirstOrderCartesian<2,float, 1000>);
	t_uint num_iterations = problem.solveHJB(&method::SemiOrderedFastIterative<2, float, 1000>);
	std::cout << "SOFIM done in " <<  num_iterations << " iterations." << std::endl;
	saveVTK(num_iterations, "SOFI", problem);

	// auto path = application::findBacktrackPath({1.4f,1.2f}, problem);
	auto path = application::gradientDescent({-1.2f,1.2f}, problem);
	saveVTK(num_iterations, "Path", problem, path); // print found path
	// method::computeErrorsEuclidian("obstacle", problem); // takes too much time...
}

void test_gdal()
{
#ifdef GDAL_
	auto all_polygons = external::readShapeFilePolygons<double>(std::string("buildings2.shp"));
	std::cout << printContainer(all_polygons) << std::endl;

	RegGrid<2, double, 1000> grid({2.40600, 48.757760}, {2.406403, 48.758000});

	auto problem = createProblem(grid);
	auto point = createPoint(grid);
	// auto center_point = point({2.40620, 48.75790});
	auto center_point = point();
	std::cout << "center_point" << printContainer(center_point) << std::endl;
	problem.frontInitialize(center_point);

	auto polygon_create = createPolygon(grid);
	for (auto poly : all_polygons)
	{
		std::cout << printContainer(poly) << std::endl;
		auto curr_polygon = polygon_create(poly);
		problem.setObstacle(curr_polygon);
	}

	t_uint num_iterations = problem.solveHJB(&method::SemiOrderedFastIterative<2, double, 1000>);
	std::cout << "SOFIM done in " <<  num_iterations << " iterations." << std::endl;
	saveVTK(num_iterations, "SOFI", problem);
#endif
}


void test_shape_from_shading()
{
	application::Image<float> img = // application::readPGM<float>("HumanFaceBefore.pgm");
		application::readPGM<float>(std::string(config::project_path) + "/test/HumanFaceBefore.pgm"); // in the project top level
	auto vec_speed = img.giveVelocity();
	// std::cout << printContainer(vec_speed) << std::endl;
	// std::cout << printContainer(img.init()) << std::endl;

	// RegGrid<2, float, 202, 259> grid;
	RegGrid<2, float, 100, 100> grid;
	auto problem = createProblem(grid);
	// std::cout << vec_speed.size();
	// grid.print();
	problem.setVelocity(vec_speed);
	problem.frontInitialize(img.init());

	//problem.setHJBScheme(&scheme::finiteFirstOrderCartesian<2, float, 100, 100>); // can be changed
	// problem.setHJBScheme(&scheme::solveSecondOrderCartesian<2, float, 100, 100>); // can be changed
	problem.setHJBScheme(&scheme::simpleFirstOrderCartesian<2,float, 100, 100>);
	problem.setHJBMethod(&method::RouyTourin<2, float, 100, 100>); // can be changed

	t_uint num_iterations = problem.solveHJB();

	std::cout << "Shape from shading done in " <<  num_iterations << " iterations." << std::endl;
	saveVTK(num_iterations, "Shape", problem);
}

int main()
{
	// C++0x: __cplusplus > 199711L .  C++11: 201103L .  C++14: 201402L.
	std::cout << "(i) __cplusplus is " << __cplusplus << std::endl;
	// std::cout << "Grid Test 0 " << Timer<>::exec<void(void)>(test_grid_0) << " ms" << std::endl;
	// std::cout << "Grid Test 1 " << Timer<>::exec<void(void)>(test_grid_1) << " ms" << std::endl;
	// std::cout << "Grid Test 2 " << Timer<>::exec<void(void)>(test_grid_2) << " ms" << std::endl;
	// std::cout << "Grid Test 3 " << Timer<std::chrono::microseconds>::exec<void(void)>(test_grid_3) << " ms" << std::endl;

	// std::cout << "Geometry 1 " << Timer<>::exec<void(void)>(test_geometry_1) << " ms" << std::endl;
	std::cout << "@Center test@ " << Timer<>::exec<void(void)>(test_center) << " ms" << std::endl;
	std::cout << "@Chacon sphere@ " << Timer<>::exec<void(void)>(test_chacon_sphere) << " ms" << std::endl;
	// std::cout << "@Speed center@ " << Timer<>::exec<void(void)>(test_speed_center) << " ms" << std::endl;
	std::cout << "@Front obstacle@ " << Timer<>::exec<void(void)>(test_front_obstacle) << " ms" << std::endl;
	// std::cout << "@OGR test@ " << Timer<>::exec<void(void)>(test_gdal) << " ms" << std::endl;
	// std::cout << "@Shape from shading@ " << Timer<>::exec<void(void)>(test_shape_from_shading) << " ms" << std::endl;
}

