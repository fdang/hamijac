/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef OGR_H_
#define OGR_H_

#include <iostream>
#include <vector>
#include <algorithm>

// OGR GDAL Library 1.11 http://gdal.org/1.11
#include "ogrsf_frmts.h"

//! Read and transforms shapefile OGR maps into Hamijac problems
/*
	This should be fast with geometry functions compared to Par4HJB
*/
namespace external
{

template<typename T>
std::vector<std::vector<std::vector<T>>> readShapeFilePolygons(const std::string& filename)
{
	std::vector<std::vector<std::vector<T>>> all_polygons;
	T min_x = Constant<T>::k_infinity, min_y = Constant<T>::k_infinity;
	T max_x = - Constant<T>::k_infinity, max_y = - Constant<T>::k_infinity;

	 /* Opening shape file */
	OGRDataSource       *poDS;

	OGRRegisterAll();

	poDS = OGRSFDriverRegistrar::Open(filename.c_str(), false);
	if( poDS == nullptr)
	{
		std::cerr << "Open failed.\n";
		exit(1);
	}
	/* Browsing layers and then features */

	OGRLayer  *poLayer;
	poLayer = poDS->GetLayer(0);

	printf("[GDAL] Number of layers is : %d\n", poDS->GetLayerCount());
    OGRFeature *poFeature;
    poLayer->ResetReading();

	int it = 0;
	while((poFeature = poLayer->GetNextFeature()) != nullptr)
	{
		std::vector<std::vector<T>> current_polygon;
		it ++;
		/*
		OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();
		int iField;
		for( iField = 0; iField < poFDefn->GetFieldCount(); iField++ )
		{
		OGRFieldDefn *poFieldDefn = poFDefn->GetFieldDefn( iField );
		if( poFieldDefn->GetType() == OFTInteger )
		printf( "%d,", poFeature->GetFieldAsInteger( iField ) );
		else if( poFieldDefn->GetType() == OFTReal )
		printf( "%.4f,", poFeature->GetFieldAsDouble(iField) );
		else if( poFieldDefn->GetType() == OFTString )
		printf( "%s,", poFeature->GetFieldAsString(iField) );
		else
		printf( "%s,", poFeature->GetFieldAsString(iField) );
		}
		*/
		OGRGeometry *poGeometry;
		poGeometry = poFeature->GetGeometryRef();
		// printf("wkb type #%d\n", poGeometry->getGeometryType());
		OGRPolygon *poPolygon = (OGRPolygon *) poGeometry;
		printf("\n%dNum interior rings : %d\n", it, poPolygon->getNumInteriorRings());

		OGRLinearRing *poLinRing = poPolygon->getExteriorRing();
		int num_polygon_vertices = poLinRing->getNumPoints();

		printf("Num vertices exterior ring = %d\n", num_polygon_vertices);
		for (int i = 0; i < num_polygon_vertices - 1; i ++)
		{
			T x = static_cast<T>(poLinRing->getX(i));
			T y = static_cast<T>(poLinRing->getY(i));
			printf( "#%d : %.8f %.8f\n", i, x, y);
			current_polygon.push_back({x,y});
			// compute min max
			min_x = std::min(min_x, x); min_y = std::min(min_y, y);
			max_x = std::max(max_x, x); max_y = std::max(max_y, y);
		}
		// disp_farray(vPolygon.data(), vPolygon.size(), LOG_SILENT); printf("\n");
		// PHJ_obstacle(vPolygon.data(), vPolygon.size(), front_polygon, reg_grid.mesh);
		OGRFeature::DestroyFeature( poFeature );
		all_polygons.push_back(current_polygon);
	} // end while

	OGRDataSource::DestroyDataSource( poDS );

	printf("min : %f    %f   max : %f    %f\n", min_x, min_y, max_x, max_y);

	return all_polygons;
}


} // end namespace

#endif // OGR_H_

