/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef FUNCTIONAL_STYLE_H_
#define FUNCTIONAL_STYLE_H_

#include <functional>  // forward, bind...
#include <iostream>
#include <string>
 using std::string;
#include <iterator>    // const_iterator
#include <type_traits> // std::enable_if
#include <vector>
 using std::vector;
#include <valarray>
 using std::valarray; // faster than vector ?
#include <algorithm>

static constexpr size_t k_max_container_display = 1000;

template <typename T>
void removeDuplicate(std::vector<T>& vec)
{
	std::sort(vec.begin(), vec.end());
	vec.erase(unique(vec.begin(), vec.end()), vec.end());
}


template <typename Iter, typename Cont>
bool isLast(Iter iter, const Cont& cont)
{
	return (iter != cont.end()) && (next(iter) == cont.end());
}

template <typename T>
struct is_cont {
	static const bool value = false;
};

template <typename T,typename Alloc>
struct is_cont<std::vector<T,Alloc> > {
	static const bool value = true;
};

template <typename T, typename std::enable_if< !is_cont<typename T::value_type>::value>::type* = nullptr >
std::string printContainer(T const& container)
{
	if (container.size() > k_max_container_display) { return "{not displayed (too large)}"; }
	std::string str = "{";
	for (auto it = std::begin(container); it != std::end(container); ++ it)
		str += (isLast(it, container) ? std::to_string(*it) + "}" : std::to_string(*it) + ",");
	return str;
}

template <typename T, typename std::enable_if< is_cont<typename T::value_type>::value>::type* = nullptr >
std::string printContainer(T const& container)
{
	if (container.size() > k_max_container_display) { return "{not displayed (too large)}"; }
	std::string str = "{";
	for (auto it = std::begin(container); it != std::end(container); ++ it)
		str += (isLast(it, container) ? printContainer(*it) + "}" : printContainer(*it) + ",");
	return str;
}


//! e.g. passMemberFunc(classA, classB, &decltype(classA)::method_function, arg1, arg2);
template<class Class, typename Func, typename... Args>
void passMemberFunc(const Class& a, const Class& b, Func func, Args&&... args)
{
	auto f = std::bind(func, a, b, std::forward<Args>(args)...);
	Class c = f();
}


#endif // FUNCTIONAL_STYLE_H_
