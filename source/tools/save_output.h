/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef SAVE_VTK_H_
#define SAVE_VTK_H_

#include <iostream>
#include <fstream>

#include "solver/problem.h"

template <typename T>
std::string printVectorVTK(T const& container)
{
	std::string str;
    for (auto it = std::begin(container); it != std::end(container); ++ it)
	{
		if (std::isinf(*it))
			str += std::to_string(-1) + " ";
		else
			str += std::to_string(*it) + " ";
	}
    return str;
}


template<t_uint DIM, typename RealT, t_uint... v>
void saveCSV(const t_uint it, const std::string& label, const Problem<DIM, RealT, v...>& problem)
{
	std::cout << "# saving iteration " << it << " CSV file " << label << "...";
	std::string filename = label + "_" + std::to_string(DIM) + "d_" + std::to_string(it) + ".csv";
	std::ofstream file(filename.c_str(), std::ios::out | std::ios::trunc);
	file << printVectorVTK(problem.u());
	std::cout << " [done]" << std::endl;
}


//! saveVTK givent value function and a problem environment. Value function can be different to the one stored in problem. 
template<t_uint DIM, typename RealT, t_uint... v>
void saveVTK(const t_uint it, const std::string& label, const Problem<DIM, RealT, v...>& problem, const vector<RealT>& grid_values)
{
	std::cout << "# saving iteration " << it << " VTK file " << label << "...";
	std::string filename = label + "_" + std::to_string(DIM) + "d_" + std::to_string(it) + ".vtk";
	std::ofstream file(filename.c_str(), std::ios::out | std::ios::trunc);
	if (DIM > 3)
		std::cout << "Warning ! Dimensions above 3 cannot be visualize in VTK. Read the vtk file as CSV file." << std::endl;

	file << "# vtk DataFile Version 3.0\n" << label << "\nASCII\nDATASET RECTILINEAR_GRID\n";
	file << "DIMENSIONS " << problem.vertices_per_dir(0) << " " << problem.vertices_per_dir(1) << " ";
	(DIM == 3) ? file << problem.vertices_per_dir(2) : file << 1;
	file << "\nX_COORDINATES " << problem.vertices_per_dir(0) << " float\n";
	file << printVectorVTK(problem.giveRealOneDim(0));
	file << "\nY_COORDINATES " << problem.vertices_per_dir(1) << " float\n";
	file << printVectorVTK(problem.giveRealOneDim(1));
	if (DIM == 3)
	{
		file << "\nZ_COORDINATES " << problem.vertices_per_dir(2) << " float\n";
		file << printVectorVTK(problem.giveRealOneDim(2));
	}
	else
		file << "\nZ_COORDINATES 1 float\n0.0";

	file << "\nPOINT_DATA " << problem.num_vertices();
	file << "\nSCALARS u float 1\nLOOKUP_TABLE default\n";
	file << printVectorVTK(grid_values);
	std::cout << " [done]" << std::endl;
}

template<t_uint DIM, typename RealT, t_uint... v>
void saveVTK(const t_uint it, const std::string& label, const Problem<DIM, RealT, v...>& problem)
{
	saveVTK(it, label, problem, problem.u());
}



#endif // SAVE_VTK_H_

