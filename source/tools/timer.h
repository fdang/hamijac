/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <ctime>
#include <chrono>
#include <string>
#include <iostream>

#ifdef _OPENMP
#include <omp.h>
#endif


//! For time measurement
/*
	ex : std::cout << Timer<std::chrono::microseconds>::execution<bool(size_t)>(isPrime, test_number);
*/
template<typename TimeT = std::chrono::milliseconds>
struct Timer
{
	// decltype(std::declval<F>()(std::declval<Args>()...))
	// typename std::result_of<F(Args...)>::type
	template<typename F, typename ...Args>
	static typename TimeT::rep exec(F func, Args&&... args)
	{
		auto start = std::chrono::steady_clock::now();
		func(std::forward<Args>(args)...);
		auto duration = std::chrono::duration_cast<TimeT>(std::chrono::steady_clock::now() - start);
		return duration.count();
	}
};

//! For time measurement using omp_get_wtime from OpenMP
/*
	double start = omp_get_wtime();
	// do stuff
	displayTime("Time example", start);
*/
inline void displayTime(const std::string& caption, const double& start)
{
	// std::cout.precision(6);
	#ifdef _OPENMP
	double end =  omp_get_wtime();
	#else
	double end = 0.0;
	#endif
	double elapsed_time = end - start;
	std::cout << "> Time [" << caption << "] " << elapsed_time << " s" << std::endl;
	// std::cout.precision(2);
}


inline double displayTimeChrono(const std::string& caption,
	const std::chrono::time_point<std::chrono::system_clock>& start)
{
	std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
 	std::cout << "> Time [" << caption << "] " << elapsed_seconds.count() << " s\n";

    return static_cast<double>(elapsed_seconds.count());
}

// Careful this is the CPU time not Wall time !!!
inline double displayTimeClock(const std::string& caption, const std::clock_t& begin_time)
{
	double elapsed_time = static_cast<double>(std::clock() - begin_time) / CLOCKS_PER_SEC;
	// std::cout.precision(6);
	std::cout << "> Time [" << caption << "] " << elapsed_time << " s\n";
	// std::cout.precision(2);
	return elapsed_time;
}

void waitKey() 
{
	std::cin.get();
}

#endif // TIMER_H_
