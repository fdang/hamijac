/* Hamijac : Multi-level parallel Hamilton-Jacobi-Bellman library
 * Copyright (c) 2014 Florian Dang <florian.coin@gmail.com>
 *
 * License MIT see file LICENSE for copying permissions
 */

#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

#include <limits>
#include "config.h"

#ifndef _DNDEBUG
#define DEBUG_BOOL_ 1
#else
#define DEBUG_BOOL_ 0
#endif

// This is deprecated in release mode assert is not visible
#define my_assert_(x) if (DEBUG_BOOL_) assert(x)
#define debug_print_(fmt, ...) do {                   \
if (DEBUG_BOOL_) fprintf(stdout, fmt, __VA_ARGS__);   \
} while (0)


typedef int         t_int;    // int by default
typedef unsigned    t_uint;   // std::size_t by default

template<typename T=double>
class Constant
{
public:
	// static constexpr t_uint k_infinite = 1.0E12;
	static constexpr T k_infinity      = std::numeric_limits<T>::infinity();
	static constexpr T k_epsilon       = std::numeric_limits<T>::epsilon();
	static constexpr T k_pi            = 3.14159265358979; // careful float precision
	// static constexpr T k_pi           = 3.141592653589793238463  // double precision
};

namespace Message
{
	const std::string m_err_template = "Error ! RegGrid template is not well written number"
		"of vertices should be equal to 0,1 or DIM";
};

#endif // TYPEDEFS_H_
