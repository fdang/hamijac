#ifndef HAMIJAC_H_
#define HAMIJAC_H_

#if defined _WIN32
# ifdef HAMIJAC_EXP
#  define HAMIJAC_API __declspec(dllexport)
# else
#  define HAMIJAC_API __declspec(dllimport)
# endif
#else
# define HAMIJAC_API  __attribute__ ((visibility ("default")))
#endif

#include "tools/timer.h"
#include "solver/problem.h"
#include "solver/method/fast_marching.h"
#include "solver/method/fast_iterative.h"
#include "solver/method/rouy_tourin.h"
#include "solver/method/sofi.h"
#include "solver/method/euclidian_distance.h"
#include "tools/save_output.h"

#include "solver/speed/speed_function.h"
#include "solver/front/geometry_front.h"

#include "application/path_finding.h"
#include "application/shape_from_shading.h"

#include "geometry/point.h"
#include "geometry/ball.h"
#include "geometry/hypercube.h"
#include "geometry/polygon2d.h"
#include "geometry/segment.h"

#ifdef GDAL_
#include "external/read_shapefile.h"
#endif

#endif
